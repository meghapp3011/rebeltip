from django.db import models
import uuid

class CustomIDModel(models.Model):
    id = models.CharField(max_length=50, unique=True, editable=False, primary_key=True)

    def save(self, *args, **kwargs):
        prefix = getattr(self, 'id_prefix', None)
        suffix = str(uuid.uuid4()).replace("-", "")
        if prefix:
            value = f'{prefix}_{suffix}'
        else:
            value = str(uuid.uuid4()).replace("-", "")
        if not self.id:
            self.id = value
        super().save(*args, **kwargs)

    def __str__(self):
        return self.id

    class Meta:
        abstract = True
