import boto3
from django.conf import settings
from django.core.mail import send_mail
import json
import os


def get_aws_parameters(name):
    ssm = boto3.client(
        'ssm',
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        region_name=settings.AWS_SES_REGION_NAME
    )
    parameter = ssm.get_parameter(Name=name, WithDecryption=True)
    data = parameter['Parameter']['Value']

    json_data = json.loads(data)
    return json_data


def send_email(email, subject, message, html_message):
    """
    Send mail to the given mail address
    """
    from_email = 'info@rebeltip.com'
    recipient_list = [email]
    response = send_mail(
        subject, message,
        from_email, recipient_list, html_message=html_message
    )


def get_file_extension(file_name):
    base_name, extension = os.path.splitext(file_name)
    return extension[1:] if extension else None
