from twilio.rest import Client
import os
from django.conf import settings


# def send_sms_with_twilio(to_phone_number, otp):
#     account_sid = os.getenv("TWILIO_ACCOUNT_SID")
#     auth_token = os.getenv("TWILIO_AUTH_TOKEN")
#     twilio_phone_number = os.getenv("TWILIO_PHONE_NUMBER")

#     client = Client(account_sid, auth_token)

#     # Your destination phone number
#     to_phone_number = f"+{to_phone_number}"

#     message_body = f"Twilio Test Otp-RebelTip: {otp}"
#     try:
#         client.messages.create(
#             to=to_phone_number,
#             from_=twilio_phone_number,
#             body=message_body
#         )
#     except Exception as e:
#         print(str(e))

def send_sms_with_twilio(to_phone_number, otp):
    if str(to_phone_number)[2:].startswith("91"):
        to_phone_number = str(to_phone_number)[2:]
    else:
        account_sid = settings.TWILIO_ACCOUNT_SID
        auth_token = settings.TWILIO_AUTH_TOKEN
        twilio_phone_number = settings.TWILIO_PHONE_NUMBER

        client = Client(account_sid, auth_token)

        message_body = f"Rebel Tip mobile verification code: {otp}"
        try:
            client.messages.create(
                to=to_phone_number,
                from_=twilio_phone_number,
                body=message_body
            )
        except Exception as e:
            print(str(e))
