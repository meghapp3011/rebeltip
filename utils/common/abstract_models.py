from django.db import models


class SoftDelete(models.Model):
    """
    Abstract model with soft delete field and method.

    Attrubutes:
        is_deleted (bool): Whether the object is soft deleted or not.
    """
    is_deleted = models.BooleanField(null=False, default=False)

    class Meta:
        abstract = True

    def delete(self, soft=None, using=None, keep_parents=False):
        if soft is not False:
            self.is_deleted = True
            self.save()
        else:
            super().delete(using=using, keep_parents=keep_parents)

    def restore(self, using=None, keep_parents=False):
        self.is_deleted = False
        self.save()
