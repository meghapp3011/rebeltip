import requests
import json

from django.conf import settings
import os


class CrunchIntegrationMixin:

    base_url = settings.CRUNCH_BASE_URL

    header = {
        'accept': 'application/json',
        'Content-Type': 'application/json-patch+json',
    }

    AuthenticationHeader = {
        "PassName": settings.CRUNCH_PASS_NAME,
        "PassCode": settings.CRUNCH_PASSCODE,
        "ClientPassCode": settings.CRUNCH_CLIENT_PASSCODE
    }

    def crunch_api_request(self, data, main_url):
        data['AuthenticationHeader'] = self.AuthenticationHeader
        url = self.base_url + main_url
        json_data = json.dumps(data, default=str)

        response = requests.post(url, headers=self.header, data=json_data)

        return response.json()

    def crunch_account_create(self):
        url = 'CrunchCard/API_CRN_CreateCardholder/'
        data = {
            "FirstName": self.first_name,
            "Surname": self.last_name,
            "DOB": self.dob,
            "Gender": " ",
            "Email": self.email,
            "MobileNumber": ('+' +
                             self.phone_country.std_code +
                             '-' +
                             self.phone),
            "Address1": self.address,
            "Town": " ",
            "Country": self.nationality.iso_code,
            "Nationality": self.nationality.iso_code,
            "CheckEmailAlert": 0,
            "PasscodeEmailNotf": 0,
            "LoadcardEmailNotf": 0,
            "CardType": 1,
            "ImageId": "",
            "IsReloadable": 0,
            "Type": "1",
            "Delv_AddrL1": self.address,
            "Delv_City": " "
        }

        response = self.crunch_api_request(data, url)

        return response

    def crunch_id3_check(self):
        url = 'ID3/API_CRN_ID3/'
        data = {
            "ProfileID": settings.CRUNCH_PROFILE_ID_FOR_PP_CHECK,
            "CardholderRef": self.crunch_detail.card_holder_ref
        }

        response = self.crunch_api_request(data, url)

        return response

    def crunch_ekyc_verification(self):
        """
        To ekyc verification
        """
        url = 'ID3/API_CRN_ID3/'
        data = {
            "ProfileID": settings.CRUNCH_PROFILE_ID_FOR_EKYC,
            "CardholderRef": self.crunch_detail.card_holder_ref
        }

        response = self.crunch_api_request(data, url)

        return response

    def crunch_upload_kyc(self):
        """
        To upload kyc
        """
        url = self.base_url + 'IDScan/API_CRN_IDScan'

        pass_name = settings.CRUNCH_PASS_NAME
        pass_code = settings.CRUNCH_PASSCODE
        client_pass_code = settings.CRUNCH_CLIENT_PASSCODE
        journey_def_id = settings.CRUNCH_JOURNEY_DEFINITION_ID

        params = {
            "AuthenticationHeader.PassName": pass_name,
            "AuthenticationHeader.PassCode": pass_code,
            "AuthenticationHeader.ClientPassCode": client_pass_code,
            "CardholderRef": self.crunch_detail.card_holder_ref,
            "JourneyDefinitionId": journey_def_id,
            "PersonEntryId": '',
            "FileType": 1
        }

        uploaded_file = self.user_kyc_file.all().order_by('created_on').last()

        pp_file = uploaded_file.pp_file
        poa_file = uploaded_file.poa_file

        files_array = [pp_file, poa_file]
        i = 1
        for file in files_array:
            filepath = os.path.join(settings.MEDIA_ROOT, file.name)
            files = {'Files': (file.name, filepath, 'multipart/form-data')}
            params["FileType"] = i

            response = requests.post(url, params=params, files=files)
            data = response.json()

            is_error = False if data['actionCode'] == "000" else True

            if is_error and i == 1:
                response = requests.post(url, params=params, files=files)
                data = response.json()
                is_error = False if data['actionCode'] == "000" else True

            self.crunch_response_store('kyc_upload' ,data['actionCodeDescription'])

            if data['actionCode'] == "000":
                params['PersonEntryId'] = data['resultIdScan']['personEntryId']
            else:
                # data['resultIdScan']['cardholderStatus'] = 'Unverified'
                return data

            i += 2

        return data


    def crunch_add_card(self):
        url = 'CrunchCard/API_CRN_AddCard/'
        data = {
            "CardholderRef": self.crunch_detail.card_holder_ref,
            "CardType": 5,
            "ProductId": settings.CRUNCH_PRODUCT_ID,
            "Recipient_Mob": ('+' +
                              self.phone_country.std_code +
                              '-' +
                              self.phone),
            "PiNMailer": 0,
            "isPreloadedCard": 0,
            "IsReloadable": 0,
            "Delv_City": self.nationality.name
        }

        response = self.crunch_api_request(data, url)

        return response

    def crunch_wallet_load(self, amount, loaded_by):
        url = 'API_CRN_Load/'
        data = {
            "PublicToken": self.crunch_detail.public_token,
            "LoadAmount": amount,
            "LoadedBy": loaded_by,
            # "Description": "",
            "SecurityCode": settings.CRUNCH_SECURITY_CODE
        }
        response = self.crunch_api_request(data, url)
        return response

    def crunch_wallet_unload(self, amount, unloaded_by):
        url = 'API_CRN_UnLoad/'
        data = {
            "PublicToken": self.crunch_detail.public_token,
            "UnLoadAmount": amount,
            "LoadedBy": unloaded_by
            # "Description": "sample description"
        }
        response = self.crunch_api_request(data, url)
        return response

    def crunch_change_card_settings(self):
        url = 'CrunchMobileApi/API_CRN_ChangeCardSettings/'
        data = {
            "SettingsType": "1",
            "CardNumber": self.crunch_detail.public_token,
            "StatusCode": "05"
        }
        response = self.crunch_api_request(data, url)
        return response
