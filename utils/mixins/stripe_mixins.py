import stripe
import uuid
import time
from django.conf import settings


class StripeIntegrationMixin:

    stripe.api_key = settings.STRIPE_SECRET_KEY

    def idempotency_key_generate(self):
        idempotency_key = uuid.uuid4()
        return str(idempotency_key)

    def stripe_connection_account_create(self):
        # user_id = "{:010d}".format(self.id)

        account = stripe.Account.create(
            type='custom',
            country='GB',
            email=self.email,
            business_type='individual',
            business_profile={
                'mcc': '4900',
                'name': self.id,
                # 'url': 'https://rebeltip.com',
                'url': settings.REDIRECTION_URL
            },

            company={
                "address": {
                    "city": getattr(self, 'city', 'sample city'),
                    "country": "GB",
                    "line1": getattr(self, 'address', 'sample line1'),
                    "postal_code": "PO16 7GZ",
                    "state": "CA"
                },
            },

            tos_acceptance={
                'date': int(time.time()),
                'ip': getattr(self, 'ip_address', '8.8.8.8'),
            },
            capabilities={"card_payments": {"requested": True},
                          "transfers": {"requested": True}},

            individual={
                'address': {
                    'city': getattr(self, 'city', 'sample city'),
                    'country': 'GB',
                    'line1': getattr(self, 'address', 'sample line1'),
                    'postal_code': 'PO16 7GZ',
                    'state': 'CA',
                },
                'dob': {
                    'day': self.dob.day,
                    'month': self.dob.month,
                    'year': self.dob.year,
                },
                'email': self.email,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'phone': f'+{self.phone_country.std_code}{self.phone}',
            },
            settings={"payouts": {"schedule": {"interval": "manual"}}},
        )
        return account

    def stripe_connection_account_delete(self):
        try:
            stripe.Account.delete(
                self.stripe_detail.connected_account_id
            )
        except:
            pass
        return True

    def stripe_mobile_update(self):
        account = stripe.Account.modify(
            self.stripe_detail.connected_account_id,
            individual={
                'phone': f'+{self.phone_country.std_code}{self.phone}',
            },
        )
        return account

    def stripe_email_update(self):
        account = stripe.Account.modify(
            self.stripe_detail.connected_account_id,
            email=self.email,
            individual={
                'email': self.email,
            },
        )
        return account

    def stripe_add_bank_account(self, data):
        bank_account_details = {
            'object': 'bank_account',
            'country': 'GB',
            'currency': 'gbp',
            'account_holder_name': data['account_holder_name'],
            'account_holder_type': 'individual',
            'routing_number': data['routing_number'],
            'account_number': data['account_number'],
            'default_for_currency': True
        }

        account = stripe.Account.create_external_account(
            self.stripe_detail.connected_account_id,
            external_account=bank_account_details,
        )
        return account

    def stripe_delete_bank_account(self):
        stripe.Account.delete_external_account(
            self.stripe_detail.connected_account_id,
            self.bank_account_details.bank_account_id,
        )
        return True

    def stripe_upload_file(self, doc_file):
        file = stripe.File.create(
            purpose='identity_document',
            file=doc_file,
            stripe_account=self.stripe_detail.connected_account_id,
        )
        return file

    def stripe_file_assign_to_user(self, data):
        if len(data) == 2:
            document = {
                "front": data[0],
                "back": data[1],
            }
        else:
            document = {
                "front": data[0],
            }

        account = stripe.Account.modify_person(
            self.stripe_detail.connected_account_id,
            self.stripe_detail.person_id,
            verification={
                "document": document
                },
        )
        return account

    def stripe_create_payment_intent(self, amount):
        # for custom checkout ui
        connect_account_id = self.stripe_detail.connected_account_id
        payment_intent = stripe.PaymentIntent.create(
            amount=int(amount*100),
            currency="gbp",

            application_fee_amount=int(amount*10),

            # payment_method='pm_card_visa',

            automatic_payment_methods={"enabled": True},
            # confirmation_method='manual',
            # confirm=True,
            transfer_data={'destination': connect_account_id},
            # return_url='https://rebeltip.com/',
            # stripe_account=connect_account_id
        )
        return payment_intent

    def stripe_connect_account_balance_transation(self, limit):
        balance_transactions = stripe.BalanceTransaction.list(
            # stripe_account=self.stripe_detail.connected_account_id,
            limit=limit
        )
        return balance_transactions

    def stripe_connect_account_payout(self, amount):

        payout = stripe.Payout.create(
            amount=amount,
            currency="gbp",
            method="instant",
            stripe_account=self.stripe_detail.connected_account_id,
        )
        return payout

    def stripe_checkout_session_for_main_account_create(self, amount):
        connect_account_id = self.stripe_detail.connected_account_id
        user_name = (
            self.first_name.capitalize() +
            ' ' +
            self.last_name.capitalize()
        )
        session = stripe.checkout.Session.create(
            idempotency_key=self.idempotency_key_generate(),
            payment_method_types=['card'],
            line_items=[{
                'price_data': {
                    'currency': 'gbp',
                    'product_data': {
                        'name': f'Tip for {user_name}',
                    },
                    'unit_amount': int(amount),
                },
                'quantity': 1,
            }],
            # payment_intent_data={
            #     'application_fee_amount': int(amount*10),
            # },
            mode='payment',
            # stripe_account=connect_account_id,
            success_url=settings.REDIRECTION_URL+'/tipper/tipping-success',
            cancel_url=settings.REDIRECTION_URL+'/tipper/tipping-unsuccess',

            metadata={
                'connected_account_id': connect_account_id
            },

        )

        return session

    def stripe_transfer_amount_to_user_account(self, amount):
        connect_account_id = self.stripe_detail.connected_account_id
        transfer = stripe.Transfer.create(
            amount=amount,
            currency="gbp",
            destination=connect_account_id
        )
        return transfer
