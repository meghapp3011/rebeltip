import os
from channels.auth import AuthMiddlewareStack
from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
import RebelTip.routing

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'RebelTip.settings')

# application = get_asgi_application()

application = ProtocolTypeRouter({
    'http': get_asgi_application(),
    'websocket': URLRouter(
            RebelTip.routing.websocket_urlpatterns
        )
})
