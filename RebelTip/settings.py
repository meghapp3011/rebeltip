from pathlib import Path
import os
from dotenv import load_dotenv
from datetime import timedelta, datetime

from utils.common.helpers import get_aws_parameters

import boto3

load_dotenv()


EMAIL_BACKEND = 'django_ses.SESBackend'

AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')

AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')

AWS_SES_REGION_NAME = os.getenv('AWS_SES_REGION_NAME')

AWS_SES_REGION_ENDPOINT = os.getenv('AWS_SES_REGION_ENDPOINT')

AWS_PARAMETER_NAME = os.getenv('AWS_PARAMETER_NAME')

AWS_S3_REGION_NAME = os.getenv('AWS_SES_REGION_NAME')


data = get_aws_parameters(AWS_PARAMETER_NAME)

AWS_STORAGE_BUCKET_NAME = data['aws_s3_bucket_name']

AWS_QUERYSTRING_AUTH = False

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = data['secret_key']

IS_SECURE = data['is_secure']


def upload_log_to_s3(log_file_path, s3_folder):
    try:
        # Create 'app' folder inside 'logs' folder
        s3_folder = os.path.join(s3_folder, 'app')

        # Generate filename with current date
        today = datetime.now().strftime('%Y-%m-%d')
        s3_file_name = f"{today}.log"

        s3_key = os.path.join(s3_folder, s3_file_name)

        s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=AWS_SECRET_ACCESS_KEY, region_name=AWS_S3_REGION_NAME)
        with open(log_file_path, 'rb') as file:
            s3.put_object(
                Body=file, Bucket=AWS_STORAGE_BUCKET_NAME, Key=s3_key)
    except Exception as e:
        pass



if IS_SECURE:
    PROTOCOL = 'https'
    DEBUG = False

    # STATIC_PATH = 'staticfiles'
    # STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
    # STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
    # STATIC_URL = 'https://%s.s3.amazonaws.com/%s/' % (AWS_STORAGE_BUCKET_NAME, STATIC_PATH)

    # MEDIA_PATH = 'media'
    # MEDIA_URL = 'https://%s.s3.amazonaws.com/%s/' % (AWS_STORAGE_BUCKET_NAME, MEDIA_PATH)
    # MEDIA_ROOT = ''

    STORAGES = {
        "default": {
            "BACKEND": "storages.backends.s3.S3Storage",
            "OPTIONS": {
                "bucket_name": AWS_STORAGE_BUCKET_NAME,
                "location": "media",
            },
        },
        "staticfiles": {
            "BACKEND": "storages.backends.s3.S3Storage",
            "OPTIONS": {
                "bucket_name": AWS_STORAGE_BUCKET_NAME,
                "location": "static", 
            },
        },
    }


    # DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

    # log_file_path = os.path.join(BASE_DIR, 'logs/app.log')

    # s3_folder = 'logs'

    # upload_log_to_s3(log_file_path, s3_folder)
else:
    PROTOCOL = 'http'
    DEBUG = True

    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')



    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, 'static'),
    ]

# ALLOWED_HOSTS = ['*']

ALLOWED_HOSTS = [
    "127.0.0.1",
    "localhost",
    "13.49.243.77",
    "dev.rebeltip.com",
    "api.rebeltip.com",
    "app.rebeltip.com",
    "10.10.26.5"
]

CSRF_TRUSTED_ORIGINS = [
    "https://dev.rebeltip.com:8001",
    "https://api.rebeltip.com"
]


# Application definition

INSTALLED_APPS = [
    'daphne',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'applications.master',
    'applications.user',
    'applications.support',
    'applications.account',
    'applications.notification',
    'applications.tipper',
    'applications.management',
    'rest_framework',
    'django_filters',
    'rest_framework_simplejwt',
    'rest_framework_simplejwt.token_blacklist',
    'drf_yasg',
    'corsheaders',
    'channels',
    'ckeditor',
    'django_logging',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'utils.common.custom_renderer.ExceptionHandlerMiddleware',
    'django_logging.middleware.DjangoLoggingMiddleware',
]

ROOT_URLCONF = 'RebelTip.urls'
AUTH_USER_MODEL = 'user.User'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'RebelTip.wsgi.application'
ASGI_APPLICATION = 'RebelTip.asgi.application'


CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            'hosts': [('127.0.0.1', 6379)],
        },
    },
}

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Bold', 'Italic', 'Underline'],
            ['Link', 'Unlink'],
            ['NumberedList', 'BulletedList'],
            ['TextColor'],
            ['Image'],
            ['Source'],
        ],
        'extraPlugins': ','.join(['justify', 'colorbutton']),
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': data['db_name'],
        'USER': data['db_user'],
        'PASSWORD': data['db_password'],
        'HOST': data['db_host'],
        'PORT': data['db_port'],
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/5.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


# Default primary key field type
# https://docs.djangoproject.com/en/5.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


REST_FRAMEWORK = {

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'utils.common.custom_renderer.CustomPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_RENDERER_CLASSES': [
        'utils.common.custom_renderer.CustomJSONRenderer',
        'rest_framework.renderers.JSONRenderer',
    ],
    'DEFAULT_FILTER_BACKENDS': [
        'django_filters.rest_framework.DjangoFilterBackend'
    ],
    # 'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',

}

CRUNCH_EMAIL_ID = data['crunch_email_id']

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(
        minutes=int(data['access_token_lifetime'])
    ),
    "REFRESH_TOKEN_LIFETIME": timedelta(
        minutes=int(data['refresh_token_lifetime'])
    ),
    "ROTATE_REFRESH_TOKENS": False,
    "BLACKLIST_AFTER_ROTATION": False,
    "UPDATE_LAST_LOGIN": True,

    "AUTH_HEADER_TYPES": ("Bearer",),
    "AUTH_HEADER_NAME": "HTTP_AUTHORIZATION"
}

CRUNCH_BASE_URL = data['crunch_base_url']
CRUNCH_CLIENT_PASSCODE = data['crunch_client_passcode']
CRUNCH_PASSCODE = data['crunch_passcode']
CRUNCH_PASS_NAME = data['crunch_pass_name']
CRUNCH_PROFILE_ID_FOR_PP_CHECK = data['crunch_profile_id_for_pp_check']
CRUNCH_PRODUCT_ID = data['crunch_product_id']
CRUNCH_PROFILE_ID_FOR_EKYC = data['crunch_profile_id_for_ekyc_id3']
CRUNCH_JOURNEY_DEFINITION_ID = data['crunch_journey_definition_id_idscan']
CRUNCH_SECURITY_CODE = data['crunch_security_code']


SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH': False,
    'JSON_EDITOR': True,
    'SHOW_REQUEST_HEADERS': True,
    'DOC_EXPANSION': 'list',
    'APIS_SORTER': 'alpha',
    'OPERATIONS_SORTER': 'alpha',
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header',
        },
    },
}

CORS_ALLOWED_ORIGINS = [
    'http://localhost:4200',
    'http://localhost',
    'http://127.0.0.1:4200',
    'http://13.49.243.77',
    "https://dev.rebeltip.com",
    "https://app.rebeltip.com",
    "https://api.rebeltip.com",
    # "https://dev.rebeltip.com:8001",
    "https://10.10.26.5"
]

CORS_ORIGIN_WHITELIST = [
    'http://localhost:4200',
    'http://localhost',
    'http://127.0.0.1:4200',
    'http://13.49.243.77',
    "https://dev.rebeltip.com",
    "https://app.rebeltip.com",
    "https://api.rebeltip.com",
    # "https://dev.rebeltip.com:8001",
    "https://10.10.26.5"
]

CORS_ALLOW_METHODS = [
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
]
CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
]

CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOW_CREDENTIALS = True

REDIRECTION_URL = data['redirection_url']
OTP_VALID_TIME = data['otp_valid_time']

STRIPE_PUBLIC_KEY = data['stripe_public_key']
STRIPE_SECRET_KEY = data['stripe_secret_key']

TWILIO_ACCOUNT_SID = data['twilio_account_sid']
TWILIO_AUTH_TOKEN = data['twilio_auth_token']
TWILIO_PHONE_NUMBER = data['twilio_phone_number']

SDD_ANNUAL_MAX_AMOUNT_LOAD = data['sdd_annual_max_amount_load']
SDD_ANNUAL_MAX_TIME_LOAD = data['sdd_annual_max_time_load']
SDD_DAILY_MAX_AMOUNT_LOAD = data['sdd_daily_max_amount_load']
SDD_DAILY_MAX_TIME_LOAD = data['sdd_daily_max_time_load']
SDD_MAX_AMOUNT_PER_LOAD = data['sdd_max_amount_per_load']
ANNUAL_MAX_AMOUNT_LOAD = data['annual_max_amount_load']
ANNUAL_MAX_TIME_LOAD = data['annual_max_time_load']
DAILY_MAX_AMOUNT_LOAD = data['daily_max_amount_load']
DAILY_MAX_TIME_LOAD = data['daily_max_time_load']
MAX_AMOUNT_PER_LOAD = data['max_amount_per_load']

STRIPE_CHECKOUT_SECRET = data['stripe_checkout_webhook_secret']
STRIPE_PAYOUT_SECRET = data['stripe_payout_webhook_secret']

# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'handlers': {
#         'app': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': 'logs/app.log',
#             'filters': ['exclude_elb_health_checker'],
#         },
#         'sql': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': 'logs/sql.log',
#         },
#         'debug': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': 'logs/debug.log',
#         },
#     },
#     'loggers': {
#         'django.db.backends': {
#             'handlers': ['sql'],
#             'level': 'DEBUG',
#             'propagate': False,
#         },
#         'django': {
#             'handlers': ['debug'],
#             'level': 'DEBUG',
#             'propagate': True,
#         },
#         'applications': {
#             'handlers': ['app'],
#             'level': 'DEBUG',
#             'propagate': True,
#         },
#     },
#      'filters': {
#         'exclude_elb_health_checker': {
#             '()': 'RebelTip.filters.ExcludeELBHealthCheckerFilter',
#         },
#     },
# }


# Set your AWS credentials

