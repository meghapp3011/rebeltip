import logging
import json

class ExcludeELBHealthCheckerFilter(logging.Filter):
    def filter(self, record):
        try:
            log_data = json.loads(record.getMessage())
            for key in log_data['WARNING']:
                request = log_data['WARNING'][key]['request']
                if 'meta' in request and 'http_user_agent' in request['meta']:
                    user_agent = request['meta']['http_user_agent']
                    if 'ELB-HealthChecker' in user_agent:
                        return False  
        except Exception:
            pass
        return True  