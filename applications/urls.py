from django.urls import path, include


urlpatterns = [
    path('user/', include('applications.user.urls')),
    path('master/', include('applications.master.urls')),
    path('support/', include('applications.support.urls')),
    path('account/', include('applications.account.urls')),
    path('notification/', include('applications.notification.urls')),
    path('tipper/', include('applications.tipper.urls')),
    path('management/', include('applications.management.urls'))
]
