from applications.notification.serializers import NotificationSerializer
from applications.notification.models import Notification
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action


class NotificationViewSet(viewsets.ViewSet):
    serializer_class = NotificationSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = Notification.objects.filter(
            user=request.user
        ).order_by('-created_on')
        serializer = self.serializer_class(
            queryset, many=True, context={'request': request}
        )
        return Response(serializer.data)

    @action(detail=False, methods=['GET'], url_path='count')
    def notification_count(self, request):
        queryset = Notification.objects.filter(
            is_read=False, user=request.user
        )
        count = queryset.count()
        return Response({"unread_count": count}, status=status.HTTP_200_OK)

    @action(detail=True, methods=['POST'], url_path='read')
    def read_notification(self, request, pk=None):
        try:
            notification = Notification.objects.get(
                id=pk, is_read=False, user=request.user
            )
            notification.is_read = True
            notification.save()
            serializer = NotificationSerializer(notification)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Notification.DoesNotExist:
            return Response(
                {"error": "Notification not found or already read."},
                status=status.HTTP_404_NOT_FOUND
            )
