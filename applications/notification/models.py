from django.db import models
from applications.user.models import User
from .constants import NotificationType

from utils.common.custom_id_field import CustomIDModel

# Create your models here.


class Notification(CustomIDModel):
    id_prefix = 'ntf'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='user_notifications'
    )
    type = models.CharField(
        max_length=200,
        choices=NotificationType.choices,
    )
    header = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    is_read = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
