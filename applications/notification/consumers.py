import json
from channels.generic.websocket import AsyncWebsocketConsumer
from applications.notification.models import Notification


class NotificationConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.group_name = 'notification'
        self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )
        await self.accept()

    async def get_notifications(self, event):
        if not self.user.is_authenticated:
            return
        notifications = Notification.objects.filter(user=self.user)
        notifications_json = [
            {
                'id': notification.id,
                'message': notification.message
            } for notification in notifications
        ]
        await self.send(text_data=json.dumps(
            {'notifications': notifications_json})
        )

    async def disconnect(self):
        await self.channel_layer.group_discard(
            self.group_name, self.channel_name
        )

    async def receive(self, text_data):
        try:
            text_data_json = json.loads(text_data)
            message = text_data_json.get('message')
            if message:
                event = {
                    'type': 'send_message',
                    'message': message
                }
                await self.channel_layer.group_send(
                    self.group_name, event
                )
            else:
                print("Message key not found in JSON data")
        except json.JSONDecodeError as e:
            print("Error decoding JSON:", e)

    async def send_message(self, event):
        message = event['message']
        await self.send(text_data=json.dumps({'message': message}))
