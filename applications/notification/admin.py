from django.contrib import admin
from .models import Notification

# Register your models here.


@admin.register(Notification)
class CountryModelAdmin(admin.ModelAdmin):
    list_display = ('user', 'type', 'header', 'description')
