

def create_notification(user, header, description, type):
    user.user_notifications.create(
        user=user,
        header=header,
        description=description,
        type=type
    )
