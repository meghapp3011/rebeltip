from django.db.models.signals import post_save
from django.dispatch import receiver
from applications.user.models import Transaction
from .constants import NotificationType
from applications.notification.utils.helpers import create_notification


@receiver(post_save, sender=Transaction)
def transaction_post_save(sender, instance, created, **kwargs):
    user = instance.user
    amount = '{:.2f}'.format(instance.amount/100)
    if instance.type == 'charge':
        header = f"£{amount} Tip received"
        if instance.is_display:
            description = f"from {instance.tipper}"
        else:
            description = ""
        type = NotificationType.RECEIVED
        create_notification(user, header, description, type)
    elif instance.type == 'payout' and instance.status == 'complete':
        header = "Redeemed to bank"
        description = f"£{amount} Cashout"
        type = NotificationType.PAYOUT

        create_notification(user, header, description, type)
