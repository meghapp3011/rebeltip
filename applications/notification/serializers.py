from rest_framework import serializers
from applications.notification.models import Notification


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = [
            'id', 'header', 'description',
            'type', 'is_read', 'created_on'
        ]
