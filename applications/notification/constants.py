from django.db import models


class NotificationType(models.TextChoices):
    RECEIVED = "Received", "received"
    PAYOUT = "Payout", "payout"
    KYC_ALERT = "Kyc Alert", "kyc_alert"
    VELOCITY_ALERT = "Velocity Alert", "velocity alert"
    PAYOUT_FAILED = "Payout Failed", "payout failed"
