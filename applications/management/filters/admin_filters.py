import django_filters


class AdminUserSearchFilter(django_filters.FilterSet):
    email = django_filters.CharFilter(
        field_name='email', lookup_expr='icontains',
    )
