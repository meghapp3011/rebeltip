import django_filters
from applications.user.models import Transaction
from django.db.models import Q



class TransactionSearchFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        method='filter_by_search'
    )
    start_date = django_filters.DateFilter(
        field_name='created_on', lookup_expr='gte'
    )
    end_date = django_filters.DateFilter(
        field_name='created_on', lookup_expr='lte'
    )

    class Meta:
        model = Transaction
        fields = []

    def filter_by_search(self, queryset, name, value):
        return queryset.filter(
            Q(transaction_id__icontains=value) |
            Q(user__first_name__icontains=value) |
            Q(user__last_name__icontains=value) |
            Q(user__email__icontains=value) |
            Q(user__phone__icontains=value) |
            Q(user__id__icontains=value)
        )