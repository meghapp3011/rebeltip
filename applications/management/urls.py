from django.urls import path, include
from rest_framework.routers import DefaultRouter
from applications.management.views import (
    base_views, transaction_views, trends_views, dashboard_views,
    demographic_views, fee_views
)


router = DefaultRouter()


router.register(
    r'admin', base_views.AdminManagementViewSet, basename='admin'
)
router.register(
    r'role', base_views.RoleViewSet, basename='role'
)
router.register(
    r'transactions', transaction_views.TransactionListViewSet, basename='transactions'
)

router.register(
    r'trends', trends_views.TrendsViewSet, basename='trends'
)

router.register(
    r'demographic', demographic_views.DemographicViewSet, basename='demographic'
)

router.register(
    r'fee', fee_views.FeeViewSet, basename='fee'
)

router.register(
    r'dashboard', dashboard_views.DashboardViewSet, basename='dashboard'
)

urlpatterns = [
    path('', include(router.urls)),

]
