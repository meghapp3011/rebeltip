from django.shortcuts import render
from rest_framework import viewsets, generics, status
from applications.user.models import User, UserBalance, DeleteAccount
from applications.management.serializers import transaction_serializer
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from applications.management.filters import transaction_filters
from rest_framework.decorators import action
from rest_framework.response import Response

from datetime import datetime, timedelta
from django.db import models


class DashboardViewSet(generics.ListAPIView, viewsets.ViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    sixty_days_ago = datetime.now() - timedelta(days=60)
    queryset = User.objects.filter(
        is_staff=False
    )

    def get_active_uers(self):
        sixty_days_ago = datetime.now() - timedelta(days=60)
        queryset = self.queryset.filter(
            last_login__gte=sixty_days_ago,
            is_deleted=False
        )
        
        return queryset
    
    def get_inactive_uers(self):
        sixty_days_ago = datetime.now() - timedelta(days=60)
        queryset = self.queryset.filter(
            is_deleted=False
        )
        queryset = queryset.exclude(
            last_login__gte=sixty_days_ago
        )
        
        return queryset
    
    def filter_data(self, field='date_joined'):
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)

        filters = models.Q()
        gte_field = f'{field}__gte'
        lte_field = f'{field}__lte'
        if start_date:
            filters |= models.Q(**{gte_field: start_date})

        if end_date:
            filters |= models.Q(**{lte_field: end_date})
        return filters

    def get_queryset(self):
        filters = self.filter_data()

        queryset = self.queryset.filter(filters)

        return queryset

    def list(self, request):

        avalible_user = self.get_queryset().count()

        active_uers = self.get_active_uers()

        inactive_uers = self.get_inactive_uers()

        # Get the count of active users
        active_users_count = active_uers.count()

        inactive_users_count = inactive_uers.count()

        active_uers_sum = UserBalance.objects.filter(
            user__in=active_uers
        ).aggregate(total_balance_sum=models.Sum('total_balance'))['total_balance_sum'] or 0

        inactive_uers_sum = UserBalance.objects.filter(
            user__in=inactive_uers
        ).aggregate(total_balance_sum=models.Sum('total_balance'))['total_balance_sum'] or 0

        filters = self.filter_data('updated_on')
        # deleted_accounts = DeleteAccount.objects.filter(filters).count()
        queryset = self.queryset.filter(filters)
        deleted_accounts = queryset.filter(is_deleted=True).count()

        total_user = avalible_user

        response = {
            'total_signups': total_user,
            'active_user': active_users_count,
            'inactive_user': inactive_users_count,
            'active_user_balance': active_uers_sum/100,
            'inactive_user_balance': inactive_uers_sum/100,
            'deleted_accounts': deleted_accounts
        }

        return Response(response)
