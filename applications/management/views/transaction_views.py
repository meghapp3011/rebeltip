from rest_framework import viewsets, generics, status
from applications.user.models import Transaction
from applications.management.serializers import transaction_serializer
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from applications.management.filters import transaction_filters
from rest_framework.decorators import action
from rest_framework.response import Response


class TransactionListViewSet(generics.ListAPIView, generics.RetrieveAPIView, viewsets.ViewSet):
    serializer_class = transaction_serializer.TransactionListSerializer
    filterset_class = transaction_filters.TransactionSearchFilter
    permission_classes = [IsAuthenticated, IsAdminUser]

    def list(self, request):
        queryset = Transaction.objects.order_by('-created_on')
        filter_set = transaction_filters.TransactionSearchFilter(
            request.query_params, queryset=queryset
        )
        queryset = filter_set.qs
        paginated_queryset = self.paginate_queryset(queryset)
        serializer = self.serializer_class(paginated_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, pk=None):
        try:
            transaction = Transaction.objects.get(id=pk)
            serializer = transaction_serializer.TransactionDetailSerializer(
                transaction
            )
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Transaction.DoesNotExist:
            return Response({
                'error': 'Transaction not found'},
                status=status.HTTP_404_NOT_FOUND
            )
