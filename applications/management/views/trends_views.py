from rest_framework import viewsets, generics
from applications.user.models import Transaction
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

# from django.db.models import Avg, Count, OuterRef, Subquery, Value
from django.db.models.functions import TruncDate
from datetime import timedelta, datetime

from django.db.models import Avg, Count, Sum

import calendar


class TrendsViewSet(generics.ListAPIView, viewsets.ViewSet):

    permission_classes = [IsAuthenticated, IsAdminUser]

    def list(self, request):

        start_date = request.GET.get('start_date')
        end_date = request.GET.get('end_date')

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
            end_date = datetime.strptime(end_date, '%Y-%m-%d').date()

        if not start_date and not end_date:
            start_date = datetime.now().replace(day=1).date()
            end_date = datetime.now().date()

        if not start_date:
            start_date = datetime.now().replace(day=1).date()
            if end_date:
                end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
                start_date = datetime(end_date.year, end_date.month, 1).date()

        if not end_date:
            end_date = datetime.now().date()
            if start_date:
                start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
                last_day = calendar.monthrange(start_date.year, start_date.month)[1]
                end_date = datetime(start_date.year, start_date.month, last_day).date()

        days_diff = (end_date - start_date).days + 1

        transactions = Transaction.objects.filter(
            created_on__range=(start_date, end_date)
        )

        charge = transactions.filter(type='charge')
        payout = transactions.filter(type='payout')

        transactions_by_date = charge.annotate(
            transaction_date=TruncDate('created_on')
        ).values('transaction_date').annotate(
            avg_amount=Avg('amount')/100,
            total_amount=Sum('amount')/100,
            num_transactions=Count('id')
        ).order_by('transaction_date')

        date_range = [start_date + timedelta(days=x) for x in range((end_date - start_date).days + 1)]

        transactions_dict = {t['transaction_date']: t for t in transactions_by_date}

        result = []

        for date in date_range:
            if date in transactions_dict:
                result.append(transactions_dict[date])
            else:
                result.append({
                    'transaction_date': date,
                    'avg_amount': 0,
                    'total_amount': 0,
                    'num_transactions': 0
                })

        # For amount details
        total_payments = (charge.aggregate(total_amount=Sum('amount'))['total_amount'] or 0)/100
        avg_total_payments = round(total_payments/days_diff, 2)

        total_payouts = -(payout.aggregate(total_amount=Sum('amount'))['total_amount'] or 0)/100
        avg_total_payouts = round(total_payouts/days_diff, 2)

        # For count details
        no_of_payments = charge.aggregate(total_charge=Count('id'))['total_charge'] or 0
        avg_no_of_payments = round(no_of_payments/days_diff, 2)

        no_of_payouts = payout.aggregate(total_payout=Count('id'))['total_payout'] or 0
        avg_no_of_payouts = round(no_of_payouts/days_diff, 2)

        # count_details = {
        #     'no_of_payments': no_of_payments,
        #     'avg_no_of_payments': '{:.2f}'.format(avg_no_of_payments),
        #     'no_of_payouts': no_of_payouts,
        #     'avg_no_of_payouts': '{:.2f}'.format(avg_no_of_payouts)
        # }

        # amount_details = {
        #     'total_payments': '{:.2f}'.format(total_payments),
        #     'avg_total_payments': '{:.2f}'.format(avg_total_payments),
        #     'total_payouts': '{:.2f}'.format(total_payouts),
        #     'avg_total_payouts': '{:.2f}'.format(avg_total_payouts)
        # }


        count_details = {
            'no_of_payments': no_of_payments,
            'avg_no_of_payments': avg_no_of_payments,
            'no_of_payouts': no_of_payouts,
            'avg_no_of_payouts': avg_no_of_payouts
        }

        amount_details = {
            'total_payments': total_payments,
            'avg_total_payments': avg_total_payments,
            'total_payouts': total_payouts,
            'avg_total_payouts': avg_total_payouts
        }


        response = {
            'count_details': count_details,
            'amount_details': amount_details,
            'transactions': result
        }

        return Response(response)
