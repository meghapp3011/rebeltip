from rest_framework import viewsets
from applications.management.models import Role
from applications.management.serializers import base_serializer
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from applications.management.filters import admin_filters
from applications.user.models import User
from rest_framework_simplejwt.serializers import (
    TokenRefreshSerializer, TokenBlacklistSerializer
)
from rest_framework_simplejwt.exceptions import TokenError


class AdminManagementViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    queryset = User.objects.filter(
        is_staff=True, is_deleted=False
    ).order_by('-date_joined')
    filterset_class = admin_filters.AdminUserSearchFilter
    pagination_class = None

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return base_serializer.AdminUpdateSerializer
        return base_serializer.AdminSerializer

    @action(detail=False, methods=['post'], permission_classes=[])
    def login(self, request):

        serializer = base_serializer.CustomTokenObtainPairSerializer(
            data=request.data
        )
        if serializer.is_valid():
            token_data = serializer.validated_data
            return Response(token_data)
        return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            instance.delete()
            return Response(
                {"message": "Deleted successfully."},
                status=status.HTTP_200_OK
            )
        except User.DoesNotExist:
            return Response(
                {"error": "Admin not found."},
                status=status.HTTP_404_NOT_FOUND
            )

    @action(detail=False, methods=['POST'])
    def refresh(self, request):
        """
        API Function to refresh access token with refresh token
        """
        serializer = TokenRefreshSerializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
            token_data = serializer.validated_data
            return Response(token_data)
        except TokenError as e:
            return Response({
                'refresh': str(e)}, status=status.HTTP_401_UNAUTHORIZED
            )

    @action(detail=False, methods=['POST'])
    def logout(self, request):
        """
        API Function for Invalidate User access token
        """
        serializer = TokenBlacklistSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            response = {'detail': 'Logout successfully'}
            return Response(response)
        except TokenError as e:
            return Response({
                'refresh': str(e)}, status=status.HTTP_401_UNAUTHORIZED
            )


class RoleViewSet(viewsets.ModelViewSet):
    queryset = Role.objects.all()
    serializer_class = base_serializer.RoleSimpleSerializer
