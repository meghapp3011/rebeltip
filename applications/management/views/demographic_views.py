from rest_framework import viewsets, generics
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

from applications.user.models import User

from django.db.models import Count, Sum, Value, F, Q, Case, When, CharField, ExpressionWrapper, IntegerField, DateField
from django.db.models.functions import Coalesce, ExtractDay

from django.utils import timezone

from applications.management.constants import AGE_GROUPS


# class DemographicViewSet(generics.ListAPIView, viewsets.ViewSet):

#     permission_classes = [IsAuthenticated, IsAdminUser]

#     def list(self, request):

#         field = request.query_params.get('field', None)
#         industry = request.query_params.get('industry', None)
#         resp_type = request.query_params.get('type', 'count')

#         key_name = field.split('__')[0]

#         sub = field.split('__')

#         if len(sub) > 1:
#             annotation_dict = {
#                 key_name: F(field),
#             }
#         else:
#             annotation_dict = {}

#         users = User.objects.filter(
#             is_staff=False,
#             is_deleted=False
#         )

#         if industry:
#             users = users.filter(industry=industry)

#         if key_name == 'age_group':

#             age_expr = ExpressionWrapper(
#                 ExtractDay(
#                     ExpressionWrapper(
#                         timezone.now() - F('dob'),
#                         output_field=DateField()
#                     )
#                 ) / 365.25,
#                 output_field=IntegerField()
#             )

#             users = users.annotate(
#                 age=age_expr,
#                 age_group = Case(
#                     *[When(age__range=(start, end), then=Value(group)) for start, end, group in AGE_GROUPS],
#                     default=Value('Unknown'),
#                     output_field=CharField()
#                 )
#             )

#         if resp_type == 'count':
#             annotation_dict[resp_type] = Count('id')
#         else:
#             annotation_dict[resp_type] = Coalesce(Sum('user_transaction__amount', filter=Q(user_transaction__type='charge')), 0)/100

#         if len(sub) > 1:
#             category_annotation = {
#                 field: Coalesce(field, Value('Other'))
#             }
#             industry_user_counts = users.annotate(
#                 **category_annotation
#             ).values(
#                 field
#             ).annotate(
#                 **annotation_dict
#             ).values(key_name, resp_type)
#         else:
#             industry_user_counts = users.values(
#                 field
#             ).annotate(
#                 **annotation_dict
#             ).values(key_name, resp_type)

#         return Response(industry_user_counts)


class DemographicViewSet(generics.ListAPIView, viewsets.ViewSet):

    permission_classes = [IsAuthenticated, IsAdminUser]

    def list(self, request):

        field = request.query_params.get('field', None)
        industry = request.query_params.get('industry', None)
        resp_type = request.query_params.get('type', 'count')

        key_name = field.split('__')[0]

        sub = field.split('__')

        annotation_dict = {
            'name': F(field),
        }

        users = User.objects.filter(
            is_staff=False,
            is_deleted=False
        )

        if industry:
            users = users.filter(industry=industry)

        if key_name == 'age_group':

            age_expr = ExpressionWrapper(
                ExtractDay(
                    ExpressionWrapper(
                        timezone.now() - F('dob'),
                        output_field=DateField()
                    )
                ) / 365.25,
                output_field=IntegerField()
            )

            users = users.annotate(
                age=age_expr,
                age_group=Case(
                    *[When(age__range=(start, end), then=Value(group))
                    for start, end, group in AGE_GROUPS],
                    default=Value('Unknown'),
                    output_field=CharField()
                )
            )

        if resp_type == 'count':
            annotation_dict['value'] = Count('id')
        else:
            annotation_dict['value'] = Coalesce(
                Sum('user_transaction__amount', filter=Q(user_transaction__type='charge')), 0)/100

        if len(sub) > 1:
            category_annotation = {
                field: Coalesce(field, Value('Other'))
            }
            industry_user_counts = users.annotate(
                **category_annotation
            ).values(
                field
            ).annotate(
                **annotation_dict
            ).values('name', 'value')
        else:
            industry_user_counts = users.values(
                field
            ).annotate(
                **annotation_dict
            ).values('name', 'value')

        if key_name == 'age_group':
            age_group_counts = {group[2]: 0 for group in AGE_GROUPS}

            for entry in industry_user_counts:
                age_group_name = entry['name']
                age_group_counts[age_group_name] = entry['value']

            industry_user_counts = [{"name": name, "value": count}
                                    for name, count in age_group_counts.items()]

        return Response(industry_user_counts)
