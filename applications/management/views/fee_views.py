from rest_framework import viewsets, generics
from applications.user.models import Transaction
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

# from django.db.models import Avg, Count, OuterRef, Subquery, Value
from django.db.models.functions import TruncDate
from datetime import timedelta, datetime

from django.db.models import Avg, Count, Sum

import calendar


class FeeViewSet(generics.ListAPIView, viewsets.ViewSet):

    permission_classes = [IsAuthenticated, IsAdminUser]

    def list(self, request):

        start_date = request.GET.get('start_date')
        end_date = request.GET.get('end_date')

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
            end_date = datetime.strptime(end_date, '%Y-%m-%d').date()

        if not start_date and not end_date:
            start_date = datetime.now().replace(day=1).date()
            end_date = datetime.now().date()

        if not start_date:
            start_date = datetime.now().replace(day=1).date()
            if end_date:
                end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
                start_date = datetime(end_date.year, end_date.month, 1).date()

        if not end_date:
            end_date = datetime.now().date()
            if start_date:
                start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
                last_day = calendar.monthrange(start_date.year, start_date.month)[1]
                end_date = datetime(start_date.year, start_date.month, last_day).date()

        transactions = Transaction.objects.filter(
            created_on__range=(start_date, end_date)
        )

        charge = transactions.filter(type='charge')
        payout = transactions.filter(type='payout')

        no_of_payments = charge.aggregate(no_of_charge=Count('id'))['no_of_charge'] or 0
        no_of_payouts = payout.aggregate(no_of_payouts=Count('id'))['no_of_payouts'] or 0
        total_fee = payout.aggregate(total_fee=Sum('fee'))['total_fee'] or 0

        stripe_fee = charge.aggregate(total_fee=Sum('stripe_fee'))['total_fee'] or 0

        rebeltip_fee = total_fee - stripe_fee

        response = {
            'no_of_loads': no_of_payments,
            'no_of_withdrawals': no_of_payouts,
            'total_fee': total_fee/100,
            'stripe_fee': stripe_fee/100,
            'rebeltip_fee': rebeltip_fee/100
        }

        return Response(response)
