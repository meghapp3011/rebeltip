from rest_framework import serializers
from applications.management.models import  Role


from applications.user.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.hashers import make_password


class RoleSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ['id', 'name']


####### admin login
class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'id', 'email', 'role', 'is_active',
            'is_staff', 'is_superuser'
        ]


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        user = User.objects.get(email=attrs.get("email"), is_deleted=False)

        if not user.email_verified or not user.is_staff:
            raise serializers.ValidationError(
                {'detail': ("No active user found "
                            "with the given credentials")})

        # Extra fields in response
        data['user'] = UserProfileSerializer(user).data
        data.update(data.pop('user', {}))

        print('new data : ', data)

        return data



class AdminSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')

    class Meta:
        model = User
        fields = [
            'id', 'name', 'email', 'password',
            'is_staff', 'is_superuser'
        ]

    def create(self, validated_data):
        admin_instance = User.objects.create(
            first_name=validated_data['first_name'],
            email=validated_data['email'],
            password=make_password(validated_data['password']),
            role='admin',
            email_verified=True,
            is_staff=True
        )
        return admin_instance


class AdminUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')

    class Meta:
        model = User
        fields = [
            'id', 'name', 'email'
        ]