from rest_framework import serializers
from applications.user.models import Transaction
from applications.account.models import PromiseToPay


class TransactionListSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = [
            'id', 'transaction_id',
            'name', 'user_id', 'email', 
            'phone', 'amount', 'created_on',
            'fee', 'type', 'status',
            ]

    def get_name(self, instance):
        name = f"{instance.user.first_name} {instance.user.last_name}"
        return name

    def get_email(self, instance):
        email = instance.user.email
        return email

    def get_phone(self, instance):
        phone = instance.user.phone
        return phone


class TransactionDetailSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    kyc_status = serializers.SerializerMethodField()
    balance_amount = serializers.SerializerMethodField()
    is_promise_to_pay = serializers.SerializerMethodField()
    promise_to_pay_coupon = serializers.SerializerMethodField()
    tipper_email = serializers.SerializerMethodField()
    promised_amount = serializers.SerializerMethodField()
    reason = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = [
            'id', 'transaction_id', 'created_on', 'name', 'email',
            'kyc_status', 'balance_amount', 'status', 'type', 'fee', 
            'amount', 'payment_method', 'is_promise_to_pay', 'promise_to_pay_coupon',
            'promised_amount', 'tipper_email', 'reason', 'user_id'
            ]

    def get_name(self, instance):
        name = f"{instance.user.first_name} {instance.user.last_name}"
        return name

    def get_is_promise_to_pay(self, instance):
        try:
            promise_to_pay = PromiseToPay.objects.get(
                unique_key=instance.reference_id
            )
            return True
        except PromiseToPay.DoesNotExist:
            return False

    def get_promise_to_pay_coupon(self, instance):
        promise_to_pay_coupon = instance.reference_id
        return promise_to_pay_coupon

    def get_tipper_email(self, instance):
        try:
            promise_to_pay = PromiseToPay.objects.get(
                unique_key=instance.reference_id
            )
            return promise_to_pay.tipper
        except PromiseToPay.DoesNotExist:
            return None

    def get_promised_amount(self, instance):
        try:
            promise_to_pay = PromiseToPay.objects.get(
                unique_key=instance.reference_id
            )
            return promise_to_pay.amount
        except PromiseToPay.DoesNotExist:
            return None

    def get_reason(self, instance):
        try:
            promise_to_pay = PromiseToPay.objects.get(
                unique_key=instance.reference_id
            )
            return promise_to_pay.reason
        except PromiseToPay.DoesNotExist:
            return None

    def get_email(self, instance):
        email = instance.user.email
        return email

    def get_kyc_status(self, instance):
        kyc_status = instance.user.kyc_status
        return kyc_status

    def get_balance_amount(self, instance):
        user_balance = instance.user.user_balance.total_balance
        return user_balance
