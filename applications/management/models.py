from django.db import models
from utils.common.custom_id_field import CustomIDModel
from applications.user.models import User


class Role(CustomIDModel):
    id_prefix = 'rol'
    name = models.CharField(max_length=254)
    created_on = models.DateTimeField(
        auto_now_add=True
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='created_role',
        null=True, blank=True
    )
    updated_on = models.DateTimeField(
        auto_now=True)
    updated_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='updated_role',
        null=True, blank=True
    )


