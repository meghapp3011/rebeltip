import factory
from applications.master import models as master_model


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = master_model.Country

    name = factory.Faker('country')
    iso_code = factory.Faker('numerify', text='###')
    std_code = factory.Faker('numerify', text='###')
    flag = factory.Faker('image_url')
