from rest_framework import serializers
from . import models as master_model


class CountryListSerializer(serializers.ModelSerializer):

    class Meta:
        model = master_model.Country
        fields = '__all__'


class ProfessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_model.Profession
        fields = ['id', 'name']


class IndustrySerializer(serializers.ModelSerializer):
    professions = ProfessionSerializer(
        many=True, read_only=True, source='user_profile'
    )

    class Meta:
        model = master_model.Industry
        fields = ['id', 'name', 'mcc_code', 'professions']
