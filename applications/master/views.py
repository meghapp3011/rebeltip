from rest_framework.response import Response
from rest_framework import viewsets, generics
from . import models as master_model
from .serializers import CountryListSerializer
from . import serializers as master_serializer

from django.db.models import Case, When, Value, CharField

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class CountryListView(generics.ListAPIView, viewsets.ViewSet):
    # queryset = master_model.Country.objects.all()
    serializer_class = CountryListSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = master_model.Country.objects.annotate(
            uk_first=Case(
                When(name='uk', then=Value(0)),
                When(name__icontains='united kingdom', then=Value(0)),
                default=Value(1),
                output_field=CharField(),
            )
        ).order_by('uk_first', 'name')
        return queryset

    @method_decorator(cache_page(60 * 60 * 2))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class CategoryViewSet(viewsets.ViewSet):
    serializer_class = master_serializer.IndustrySerializer

    def list(self, request):
        industries = master_model.Industry.objects.all()
        serializer = self.serializer_class(industries, many=True)
        return Response(serializer.data)
