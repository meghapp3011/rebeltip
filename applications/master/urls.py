from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views as master_view

router = DefaultRouter()

router.register(
    r'country', master_view.CountryListView, basename='country_list'
)

router.register(
    r'category', master_view.CategoryViewSet, basename='category'
)

urlpatterns = [
    path('', include(router.urls)),
]
