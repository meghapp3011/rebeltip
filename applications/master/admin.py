from django.contrib import admin
from . import models as master_model
# Register your models here.


# admin.site.register(master_model.Country)
# admin.site.register(master_model.Industry)
# admin.site.register(master_model.JobType)
# admin.site.register(master_model.Designation)


@admin.register(master_model.Country)
class CountryModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'iso_code', 'std_code')
    search_fields = ("name", "iso_code", "std_code")
    ordering = ('name',)


@admin.register(master_model.Industry)
class IndustryModelAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ("name",)
    ordering = ('name',)


# @admin.register(master_model.Designation)
# class DesignationModelAdmin(admin.ModelAdmin):
#     list_display = ('id', 'name')
#     search_fields = ("name",)
#     ordering = ('name',)


@admin.register(master_model.Profession)
class JobTypeModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ("name",)
    ordering = ('name',)
