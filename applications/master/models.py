from django.db import models
from utils.common.custom_id_field import CustomIDModel

# Create your models here.


class Country(CustomIDModel):
    id_prefix = 'cty'
    name = models.CharField(max_length=100, unique=True, null=False)
    iso_code = models.CharField(max_length=20, null=False)
    alpha_2_code = models.CharField(max_length=2, null=False)
    std_code = models.CharField(max_length=20, null=False)
    flag = models.ImageField(null=False, upload_to="flags/")

    def __str__(self):
        return self.name


class Industry(CustomIDModel):
    id_prefix = 'ind'
    name = models.CharField(max_length=150)
    mcc_code = models.CharField(max_length=10, null=False)

    def __str__(self):
        return self.name


class Profession(CustomIDModel):
    id_prefix = 'pro'
    industry = models.ForeignKey(
        Industry, on_delete=models.CASCADE,
        related_name='user_profile'
    )
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


# class Designation(CustomIDModel):
#     name = models.CharField(max_length=150)

#     def __str__(self):
#         return self.name
