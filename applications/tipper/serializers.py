from rest_framework import serializers
from applications.user import models as user_model
from applications.account import models as account_model
from applications.user.constants import KYC_STATUS_CHOICES
from applications.tipper.utils import helpers


class TippeSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    industry = serializers.SerializerMethodField()
    profession = serializers.SerializerMethodField()
    profile_pic = serializers.SerializerMethodField()

    class Meta:
        model = user_model.User
        fields = [
            'id', 'first_name', 'last_name', 'full_name',
            'industry', 'profession', 'profile_pic'
        ]

    def get_fields(self):
        fields = super().get_fields()

        if self.context.get('amount'):
            fields['amount'] = serializers.SerializerMethodField()

        return fields

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_industry(self, obj):
        if obj.industry:
            return obj.industry.name
        return None

    def get_profession(self, obj):
        if obj.profession:
            return obj.profession.name
        return None

    def get_amount(self, obj):
        amount = self.context.get('amount')
        return amount

    def get_profile_pic(self, instance):
        request = self.context.get('request')
        # domain = get_current_site(request)
        # user_profile = instance.user_profile.order_by('created_on').first()
        # protocol = settings.PROTOCOL
        # if user_profile and user_profile.profile_pic:
        #     # return user_profile.profile_pic.url
        #     return f"{protocol}://{domain}{user_profile.profile_pic.url}"
        # else:
        #     return None
        if hasattr(instance, 'user_profile'):
            # return instance.user_profile.get_profile_pic_ulr(request)
            return instance.user_profile.profile_pic.url
        else:
            return None


class CheckoutSessionSerializer(serializers.Serializer):
    user = serializers.PrimaryKeyRelatedField(
        queryset=user_model.User.objects.filter(
            is_active=True,
            is_deleted=False
        )
    )
    amount = serializers.FloatField()
    tipper_name = serializers.CharField(max_length=100)
    is_display = serializers.BooleanField(default=False)
    reason = serializers.CharField(required=False, allow_blank=True)
    unique_key = serializers.CharField(required=False)

    def validate(self, data):
        user = data.get('user')
        amount = data.get('amount')

        is_velocity_check_pass = user.velocity_check(amount)

        if (
            not is_velocity_check_pass and
            user.kyc_status == KYC_STATUS_CHOICES.New
        ):
            # perfome ekyc verification
            is_ekyc_approved = user.ekyc_verification_crunch()
            if is_ekyc_approved:
                is_velocity_check_pass = user.velocity_check(amount)
                if is_velocity_check_pass:
                    return data
            message = {
                'detail': "We were unable to complete the transaction and "
                "we’re working on it. Please try another time. Thank you "
                }
            raise serializers.ValidationError(message)

        elif not is_velocity_check_pass:
            message = {
                'detail': "We were unable to complete the transaction and "
                "we’re working on it. Please try another time. Thank you "
            }
            raise serializers.ValidationError(message)

        return data


class MainCheckoutSessionSerializer(serializers.ModelSerializer):
    unique_key = serializers.CharField(required=False, allow_blank=True)
    checkout_session_id = serializers.SerializerMethodField(required=False)
    tipper_name = serializers.CharField(required=False, allow_blank=True)
    amount = serializers.FloatField()

    class Meta:
        model = account_model.CheckoutSessions
        fields = '__all__'

    def validate_amount(self, obj):
        return obj*100

    def validate_unique_key(self, value):
        if not value:
            return None
        try:
            object = account_model.PromiseToPay.objects.get(
                unique_key=value
            )
            if object.is_redeemed:
                message = 'Code already used. '
                raise serializers.ValidationError(message)
        except account_model.PromiseToPay.DoesNotExist:
            message = 'Code entered not valid .'
            raise serializers.ValidationError(message)
        return value

    def validate(self, data):
        user = data['user']
        amount = data['amount']

        is_velocity_check_pass = user.velocity_check(amount/100)

        if (
            not is_velocity_check_pass and
            user.kyc_status == KYC_STATUS_CHOICES.New
        ):
            # perfome ekyc verification
            is_ekyc_approved = user.ekyc_verification_crunch()
            if is_ekyc_approved:
                is_velocity_check_pass = user.velocity_check(amount/100)
                if is_velocity_check_pass:
                    return data
            message = {
                'detail': "We were unable to complete the transaction and "
                "we’re working on it. Please try another time. Thank you "
                }
            raise serializers.ValidationError(message)

        elif not is_velocity_check_pass:
            message = {
                'detail': "We were unable to complete the transaction and "
                "we’re working on it. Please try another time. Thank you "
            }
            raise serializers.ValidationError(message)

        return data

    def create(self, validated_data):
        # validated_data['reference_id'] = validated_data['unique_key']
        if 'unique_key' in validated_data:
            validated_data['reference_id'] = validated_data['unique_key']
            validated_data.pop('unique_key', None)
        else:
            validated_data['reference_id'] = None

        validated_data['tipper'] = validated_data['tipper_name']

        validated_data.pop('tipper_name', None)
        validated_data['net'] = validated_data['amount']
        instance = account_model.CheckoutSessions.objects.create(
            **validated_data
        )

        return instance


class PromiseToPaySerializer(serializers.ModelSerializer):

    class Meta:
        model = account_model.PromiseToPay
        fields = ['tipper', 'amount', 'unique_key']

    def send_tipper_mail(self, instance, tipper):

        helpers.send_thankyou(instance, tipper)
        # email_thread = threading.Thread(
        #     target=helpers.send_thankyou,
        #     args=(instance, tipper)
        # )
        # email_thread.start()


class PromisedTippeeSerializer(serializers.Serializer):
    unique_key = serializers.CharField(max_length=254)

    def validate_unique_key(self, value):
        try:
            object = account_model.PromiseToPay.objects.get(
                unique_key=value, is_redeemed=False
            )
        except account_model.PromiseToPay.DoesNotExist:
            message = 'Invalid Promise to Pay code'
            raise serializers.ValidationError(message)
        return object


# class PromisedTippeeResponseSerializer(serializers.ModelSerializer):
#     full_name = serializers.SerializerMethodField()
#     industry = serializers.SerializerMethodField()
#     profession = serializers.SerializerMethodField()
#     profile_pic = serializers.SerializerMethodField()
#     amount = serializers.SerializerMethodField()

#     class Meta:
#         model = user_model.User
#         fields = [
#             'id', 'first_name', 'last_name', 'full_name',
#             'industry', 'profession', 'amount', 'profile_pic'
#         ]

#     def get_full_name(self, obj):
#         return obj.get_full_name()

#     def get_industry(self, obj):
#         if obj.industry:
#             return obj.industry.name
#         return None

#     def get_profession(self, obj):
#         if obj.profession:
#             return obj.profession.name
#         return None

#     def get_amount(self, obj):
#         amount = self.context.get('amount')
#         return amount

#     def get_profile_pic(self, instance):
#         request = self.context.get('request')
#         # domain = get_current_site(request)
#         # user_profile = instance.user_profile.order_by('created_on').first()
#         # protocol = settings.PROTOCOL
#         # if user_profile and user_profile.profile_pic:
#         #     # return user_profile.profile_pic.url
#         #     return f"{protocol}://{domain}{user_profile.profile_pic.url}"
#         # else:
#         #     return None
#         if hasattr(instance, 'user_profile'):
#             return instance.user_profile.get_profile_pic_ulr(request)
#         else:
#             return None
