from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views as tipper_view

router = DefaultRouter()

router.register(
    r'', tipper_view.TipperViewSets, basename='tipper'
)


urlpatterns = [
    path('', include(router.urls)),
]
