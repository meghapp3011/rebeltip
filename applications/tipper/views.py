from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
from applications.user import models as user_model
from . import serializers as tipper_serializer
from applications.account.models import PromiseToPay, CheckoutSessions


class TipperViewSets(viewsets.ViewSet):

    @action(detail=True, methods=['GET'])
    def tippee(self, request, pk):
        try:
            tippe = user_model.User.objects.get(
                id=pk,
                is_deleted=False
            )
        except user_model.User.DoesNotExist:
            response = {
                'detail': 'Tippe not found'
            }
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        serializer = tipper_serializer.TippeSerializer(
            tippe,
            context={'request': request}
        )
        return Response(serializer.data)

    @action(detail=False, methods=['POST'])
    def promise_to_pay(self, request):
        serializer = tipper_serializer.PromiseToPaySerializer(
            data=request.data
        )
        if serializer.is_valid():
            tipper = request.data.get('tipper')
            instance = serializer.save()
            serializer.send_tipper_mail(instance, tipper)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def promised_tippee_detail(self, request):

        serializer = tipper_serializer.PromisedTippeeSerializer(
            data=request.data
        )
        if serializer.is_valid():
            data = serializer.validated_data['unique_key']
            tippee = data.tippee
            response = tipper_serializer.TippeSerializer(
                tippee,
                context={'request': request, 'amount': data.amount
                         }
            )
            return Response(response.data)
        return Response(
            serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY
        )

    @action(detail=False, methods=['POST'])
    def payment_intent(self, request):
        serializer = tipper_serializer.CheckoutSessionSerializer(
            data=request.data
        )
        if serializer.is_valid():
            user = serializer.validated_data['user']
            amount = serializer.validated_data['amount']
            unique_key = serializer.validated_data.get('unique_key', None)
            promise_to_pay = PromiseToPay.objects.get(
                unique_key=unique_key
            ) if unique_key else None
            payment_intent = user.user_payment_intent_create_strip(amount)
            show_name = serializer.validated_data['is_display']
            tipper = serializer.validated_data.get('tipper_name', None)
            ref_id = promise_to_pay.unique_key if promise_to_pay else None
            CheckoutSessions.objects.create(
                user=user,
                checkout_session_id=payment_intent.id,
                amount=amount*100,
                is_display=show_name,
                tipper=tipper,
                reference_id=ref_id,
            )
            return Response(payment_intent)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def checkout(self, request):
        serializer = tipper_serializer.MainCheckoutSessionSerializer(
            data=request.data
        )
        if serializer.is_valid():
            user = serializer.validated_data['user']
            amount = serializer.validated_data['amount']

            session = user.main_account_checkout_session_create_stripe(amount)
            serializer.validated_data['checkout_session_id'] = session.id

            serializer.save()

            response = {
                'checkout_url': session.url,
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
