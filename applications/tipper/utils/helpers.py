# from django.core.mail import send_mail
from django.template.loader import render_to_string

from utils.common.helpers import send_email


# def send_email(email, subject, message, html_message):
#     """
#     Send mail to the given mail address
#     """
#     from_email = settings.EMAIL_HOST_USER
#     recipient_list = [email]
#     send_mail(
#         subject, message,
#         from_email, recipient_list, html_message=html_message
#     )


def send_thankyou(instance, tipper):
    amount = '{:.2f}'.format(instance.amount)
    subject = "Thank You: Rebel Tip's Promise to Pay"
    message = render_to_string('thankyou_email.html', {
        'amount': amount
    })
    send_email(
        tipper,
        subject=subject,
        message=message,
        html_message=message
    )
