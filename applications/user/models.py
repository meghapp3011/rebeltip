from django.db import models
from applications.master import models as master_model
from django.contrib.auth.models import AbstractUser
from .managers import CustomUserManager
from .constants import ROLE_CHOICES, KYC_STATUS_CHOICES
from django.conf import settings
from django.utils.crypto import get_random_string
from django.utils import timezone
from datetime import timedelta
from django.contrib.auth.hashers import make_password
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import (
    GenericForeignKey, GenericRelation
)
from django.contrib.auth.password_validation import validate_password
from utils.mixins import crunch_mixins
from django.core.exceptions import ValidationError
from utils.mixins.stripe_mixins import StripeIntegrationMixin
from applications.notification.utils import helpers
from applications.notification.constants import NotificationType

import random
import uuid

from django.db.models import Sum

from utils.common.custom_id_field import CustomIDModel

from django.contrib.sites.shortcuts import get_current_site

from django.core.files.storage import default_storage

from utils.common.helpers import get_file_extension


class UserOtp(CustomIDModel):
    id_prefix = 'otp'
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    object_id = models.CharField(max_length=100)
    user = GenericForeignKey('content_type', 'object_id')
    otp = models.IntegerField()
    created_on = models.DateTimeField(
        auto_now_add=True
    )

    class Meta:
        db_table = "users_otp"

    def is_valid(self):
        otp_validity_period = timedelta(minutes=int(settings.OTP_VALID_TIME))
        time_difference = timezone.now() - self.created_on
        return time_difference < otp_validity_period

    def save(self, *args, **kwargs):
        if not self.otp:
            # self.otp = 123456
            if hasattr(self.user, 'phone') and (
                self.user.phone.startswith('91')
                # self.user.phone_country.std_code == '91'
            ):
                self.otp = 123456
            elif hasattr(self.user, 'new_phone') and (
                # self.user.phone_country.std_code == '91'
                self.user.new_phone.startswith('91')
            ):
                self.otp = 123456
            else:
                self.otp = random.randint(100000, 999999)
            print('otp : ', self.otp)
        super(UserOtp, self).save(*args, **kwargs)


class EmailVerificationToken(CustomIDModel):
    id_prefix = 'evt'
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    object_id = models.CharField(max_length=100)
    user = GenericForeignKey('content_type', 'object_id')
    key = models.CharField(max_length=100, unique=True)
    created_on = models.DateTimeField(auto_now_add=True)
    is_used = models.BooleanField(default=False)

    def is_valid(self):
        hour_ago = timezone.now() - timedelta(hours=24)
        return hour_ago < self.created_on

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = get_random_string(32)
        super().save(*args, **kwargs)


class User(
    AbstractUser,
    CustomIDModel,
    crunch_mixins.CrunchIntegrationMixin,
    StripeIntegrationMixin
):
    id_prefix = 'usr'
    username = None
    email = models.EmailField(max_length=254, unique=True)
    email_verified = models.BooleanField(default=False)
    city = models.CharField(max_length=100)
    postal_code = models.CharField(
        max_length=10
    )
    nationality = models.ForeignKey(
        master_model.Country, on_delete=models.CASCADE, null=True, blank=True,
        related_name='user_country'
    )
    phone = models.CharField(max_length=254, null=True, blank=True)
    phone_country = models.ForeignKey(
        master_model.Country, on_delete=models.CASCADE, null=True, blank=True,
        related_name='user_phone_country'
    )
    phone_verified = models.BooleanField(default=False)
    role = models.CharField(
        max_length=200,
        choices=ROLE_CHOICES.choices,
        default=ROLE_CHOICES.User
    )
    kyc_status = models.CharField(
        max_length=200,
        choices=KYC_STATUS_CHOICES.choices,
        default=KYC_STATUS_CHOICES.New
    )
    address = models.TextField(max_length=100, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    # designation = models.ForeignKey(
    #     master_model.Designation, on_delete=models.CASCADE,
    #     null=True, blank=True,
    #     related_name='user_designation'
    # )
    industry = models.ForeignKey(
        master_model.Industry, on_delete=models.CASCADE, null=True, blank=True,
        related_name='user_industry'
    )
    profession = models.ForeignKey(
        master_model.Profession, on_delete=models.CASCADE,
        null=True, blank=True,
        related_name='user_job_type'
    )
    ip_address = models.CharField(max_length=200, null=True, blank=True)
    last_login = models.DateTimeField(default=timezone.now, blank=True, null=True)
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    groups = models.ManyToManyField(
        'auth.Group', related_name='custom_user_set_groups',
        related_query_name='custom_user_query_groups',
        blank=True
    )
    user_permissions = models.ManyToManyField(
        'auth.Permission', related_name='custom_user_set_permissions',
        related_query_name='custom_user_query_permissions',
        blank=True
    )
    user_otp = GenericRelation(UserOtp, related_query_name='user_otp')
    email_token = GenericRelation(EmailVerificationToken,
                                  related_query_name='email_token')
    
    is_deleted = models.BooleanField(default=False)
    updated_on = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.email
    
    def delete(self, soft=None, using=None, keep_parents=False):
        if soft is not False:
            unique_key = uuid.uuid4()
            self.email = self.email + str(unique_key)
            if self.phone:
                self.phone = self.phone + str(unique_key)
            self.is_deleted = True
            self.save()
        else:
            super().delete(using=using, keep_parents=keep_parents)

    # Crunch integration section
    def create_crunch_accound(self):
        account = self.crunch_account_create()
        self.crunch_response_store('create_account', account)
        if not account['actionCode'] == '000':
            self.is_active = False
            self.save()
            raise ValidationError(account['actcodedesc'])
        # self.crunch_ref_id = account['cardholderRef']
        # self.save()
        CrunchDetail.objects.create(
            user=self,
            card_holder_ref=account['cardholderRef'],
            card_holder_id=account['cardholderId'],
            username=account['username']
        )

    def check_sdd_crunch(self):
        sdd_check = self.crunch_id3_check()
        if not sdd_check['actionCode'] == '000':
            self.is_active = False
            self.save()
            raise ValidationError(sdd_check['actionCodeDescription'])

    def ekyc_verification_crunch(self):
        ekyc_verify = self.crunch_ekyc_verification()
        self.crunch_response_store('ekyc_verify', ekyc_verify)
        if ekyc_verify['result']['bandText'] == 'PASS':
            self.kyc_status = KYC_STATUS_CHOICES.Verified
            self.save()
            return True
        else:
            self.kyc_status = KYC_STATUS_CHOICES.Unverified
            self.save()
            header = (
                'Personal security checks '
                'needed for your first bank transfer'
            )
            description = ('We need to perform security checks to allow '
                           'further loads into your account. Please upload '
                           'proof of address and proof of identification.')
            notification_type = NotificationType.KYC_ALERT
            helpers.create_notification(
                self,
                header,
                description,
                notification_type
            )
            return False

    def kyc_verification_crunch(self):
        upload = self.crunch_upload_kyc()
        return upload

    def add_card_crunch(self):
        add_card = self.crunch_add_card()
        self.crunch_response_store('add_card', add_card)
        if not add_card['actionCode'] == '000':
            self.is_active = False
            self.save()
            raise ValidationError(add_card['actionCodeDescription'])
        self.crunch_detail.public_token = add_card['publicToken']
        self.crunch_detail.access_code = add_card['accCode']
        self.crunch_detail.save()

    def load_wallet_crunch(self, amount, loaded_by):
        response = self.crunch_wallet_load(amount, loaded_by)
        self.crunch_response_store('load_wallet', response)
        return response

    def unload_wallet_crunch(self, amount, unloaded_by):
        response = self.crunch_wallet_unload(amount, unloaded_by)
        self.crunch_response_store('unload_wallet', response)
        # if not response['actionCode'] == '000':
        #     raise ValidationError('unload error')
        return response

    def change_card_settings_crunch(self):
        response = self.crunch_change_card_settings()
        self.crunch_response_store('change_card_settings', response)
        return response

    # Stripe integration section
    def user_stripe_account_create(self):
        account = self.stripe_connection_account_create()
        StripeDetail.objects.create(
            user=self,
            connected_account_id=account.id,
            person_id=account.individual.id
        )
        return account

    def user_stripe_account_delete(self):
        self.stripe_connection_account_delete()
        return True

    def user_mobile_update_stripe(self):
        account = self.stripe_mobile_update()
        return account

    def user_email_update_stripe(self):
        account = self.stripe_email_update()
        return account

    def add_user_bank_account_stripe(self, data):
        account = self.stripe_add_bank_account(data)
        return account

    def delete_user_bank_account_stripe(self):
        account = self.stripe_delete_bank_account()
        return account

    def get_user_account_details_stripe(self):
        account = self.stripe_retrive_account()
        return account

    # def upload_file_stripe(self, doc_file):
    #     file = self.stripe_upload_file(doc_file)
    #     return file

    # def file_assign_to_user_stripe(self, data):
    #     account = self.stripe_file_assign_to_user(data)
    #     return account

    # def user_checkout_session_create_stripe(self, amount):
    #     session = self.stripe_checkout_session_create(amount)
    #     return session

    # def user_connect_account_balance_stripe(self):
    #     balance = self.stripe_connect_account_balance()
    #     return balance

    def user_connect_account_balance_transation_stripe(self, limit=100):
        balance_transations = self.stripe_connect_account_balance_transation(
            limit
        )
        return balance_transations

    def create_user_balance(self):
        try:
            balance = UserBalance.objects.create(user=self)
            # self.user_balance.create()
        except Exception as e:
            print("e : ", e)
        return balance

    def user_connect_account_payout_stripe(self, amount):
        payout = self.stripe_connect_account_payout(amount)
        return payout

    def user_payment_intent_create_strip(self, amount):
        payment_intent = self.stripe_create_payment_intent(amount)
        return payment_intent

    # def link_url(self):
    #     return self.account_link()

    # def create_transation(self, data):
    #     try:
    #         self.user_transaction.create(
    #             transaction_id=data.id,
    #             amount=data.amount,
    #             fee=data.fee,
    #             net=data.net,
    #             currency=data.currency,
    #             status=data.status,
    #             type=data.type,
    #             created_on=datetime.utcfromtimestamp(data['created'])
    #         )
    #     except Exception:
    #         self.user_transaction.filter(
    #             transaction_id=data.id
    #         ).update(
    #             amount=data.amount,
    #             fee=data.fee,
    #             net=data.net,
    #             currency=data.currency,
    #             status=data.status,
    #             type=data.type
    #         )

    def main_account_checkout_session_create_stripe(self, amount):
        session = self.stripe_checkout_session_for_main_account_create(amount)
        return session

    def user_transfer_create_stripe(self, amount):
        transfer = self.stripe_transfer_amount_to_user_account(
            amount
        )
        return transfer

    # Velocity limit check
    def velocity_check(self, amount):
        if (
            self.daily_limit(amount) and
            self.annual_limit(amount) and
            self.per_load_limit(amount)
        ):
            return True
        return False

    def daily_limit(self, amount):
        daily_amount_limit = (
            settings.DAILY_MAX_AMOUNT_LOAD
            if self.kyc_status == KYC_STATUS_CHOICES.Verified
            else settings.SDD_DAILY_MAX_AMOUNT_LOAD
        )
        daily_times_limit = (
            settings.DAILY_MAX_TIME_LOAD
            if self.kyc_status == KYC_STATUS_CHOICES.Verified
            else settings.SDD_DAILY_MAX_TIME_LOAD
        )
        today = timezone.now().date()

        transactions = self.user_transaction.filter(
            created_on__date=today, type='charge'
        )

        total_amount = transactions.aggregate(
            total=models.Sum('amount')
        )['total']
        total_amount = 0 if total_amount is None else total_amount
        total_amount = (total_amount / 100) + amount

        no_times = len(transactions)

        if float(daily_amount_limit) < total_amount:
            if not self.kyc_status == KYC_STATUS_CHOICES.New:
                if self.kyc_status == KYC_STATUS_CHOICES.Verified:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds daily limit.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Unverified:
                    header = 'Limit reached.'
                    description = (
                        'We need to perform security checks to allow further loads '
                        'into your account. Please upload proof of address and '
                        'proof of identification.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Pending:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds daily limit; KYC verification under process.'
                    )
                elif self.kyc_status != KYC_STATUS_CHOICES.Reject:
                    return False

                notification_type = NotificationType.VELOCITY_ALERT

                helpers.create_notification(
                    self,
                    header,
                    description,
                    notification_type
                )
            return False

        if int(daily_times_limit) < no_times:
            if not self.kyc_status == KYC_STATUS_CHOICES.New:
                if self.kyc_status == KYC_STATUS_CHOICES.Verified:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds daily limit.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Unverified:
                    header = 'Limit reached.'
                    description = (
                        'We need to perform security checks to allow further loads '
                        'into your account. Please upload proof of address and '
                        'proof of identification.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Pending:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds daily limit; KKYC verification under process.'
                    )
                elif self.kyc_status != KYC_STATUS_CHOICES.Reject:
                    return False

                notification_type = NotificationType.VELOCITY_ALERT

                helpers.create_notification(
                    self,
                    header,
                    description,
                    notification_type
                )
            return False

        return True

    def annual_limit(self, amount):
        annual_amount_limit = (
            settings.ANNUAL_MAX_AMOUNT_LOAD
            if self.kyc_status == KYC_STATUS_CHOICES.Verified
            else settings.SDD_ANNUAL_MAX_AMOUNT_LOAD
        )
        annual_times_limit = (
            settings.ANNUAL_MAX_TIME_LOAD
            if self.kyc_status == KYC_STATUS_CHOICES.Verified
            else settings.SDD_ANNUAL_MAX_TIME_LOAD
        )
        current_year = timezone.now().year

        transactions = self.user_transaction.filter(
            created_on__year=current_year, type='charge'
        )

        total_amount = transactions.aggregate(
            total=models.Sum('amount')
        )['total']
        total_amount = 0 if total_amount is None else total_amount
        total_amount = (total_amount / 100) + amount

        no_times = len(transactions)

        if float(annual_amount_limit) < total_amount:
            if not self.kyc_status == KYC_STATUS_CHOICES.New:
                if self.kyc_status == KYC_STATUS_CHOICES.Verified:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds annual limit.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Unverified:
                    header = 'Limit reached.'
                    description = (
                        'We need to perform security checks to allow further loads '
                        'into your account. Please upload proof of address and'
                        'proof of identification.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Pending:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds daily limit; KYC verification under process.'
                    )
                elif self.kyc_status != KYC_STATUS_CHOICES.Reject:
                    return False

                notification_type = NotificationType.VELOCITY_ALERT

                helpers.create_notification(
                    self,
                    header,
                    description,
                    notification_type
                )
            return False

        if int(annual_times_limit) < no_times:
            if not self.kyc_status == KYC_STATUS_CHOICES.New:
                if self.kyc_status == KYC_STATUS_CHOICES.Verified:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds annual acceptence limit.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Unverified:
                    header = 'Limit reached.'
                    description = (
                        'We need to perform security checks to allow further loads '
                        'into your account. Please upload proof of address and '
                        'proof of identification.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Pending:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible, exceeds daily '
                        'acceptence limit; KYC verification under process.'
                    )
                elif self.kyc_status != KYC_STATUS_CHOICES.Reject:
                    return False

                notification_type = NotificationType.VELOCITY_ALERT

                helpers.create_notification(
                    self,
                    header,
                    description,
                    notification_type
                )
            return False
        return True

    def per_load_limit(self, amount):
        limit = (
            settings.MAX_AMOUNT_PER_LOAD
            if self.kyc_status == KYC_STATUS_CHOICES.Verified
            else settings.SDD_MAX_AMOUNT_PER_LOAD
        )

        if float(limit) < amount:
            if not self.kyc_status == KYC_STATUS_CHOICES.New:
                if self.kyc_status == KYC_STATUS_CHOICES.Verified:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds per load limit.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Unverified:
                    header = 'Limit reached.'
                    description = (
                        'We need to perform security checks to allow further loads '
                        'into your account. Please upload proof of address and '
                        'proof of identification.'
                    )
                elif self.kyc_status == KYC_STATUS_CHOICES.Pending:
                    header = 'Limit reached.'
                    description = (
                        'Transaction not possible,'
                        ' exceeds per load limit; KYC verification under process.'
                    )
                elif self.kyc_status != KYC_STATUS_CHOICES.Reject:
                    return False

                notification_type = NotificationType.VELOCITY_ALERT

                helpers.create_notification(
                    self,
                    header,
                    description,
                    notification_type
                )
            return False
        return True

    def crunch_response_store(self, service, response):
        self.user_crunch_response.create(
            service=service,
            response=response
        )


class CrunchDetail(CustomIDModel):
    id_prefix = 'cdl'
    user = models.OneToOneField(
        User, on_delete=models.CASCADE,
        related_name='crunch_detail'
    )
    card_holder_ref = models.CharField(max_length=100)
    card_holder_id = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    public_token = models.CharField(max_length=100, null=True, blank=True)
    access_code = models.CharField(max_length=100, null=True, blank=True)


class UserBalanceManager(models.Manager):

    def get(self, *args, **kwargs):
        user = kwargs['user']
        user_balance = user.user_balance

        if not user_balance.updated_on.date() == timezone.now().date():
            self.get_avalible_to_payout(user, user_balance)
            self.get_avalible_to_payout_soon(user, user_balance)
            self.get_current_month_total(user, user_balance)
            self.get_since_last_payout(user, user_balance)
            self.get_total_volume(user, user_balance)
            self.get_total_balance(user, user_balance)
            self.get_total_payout(user, user_balance)
            self.get_total_fee(user, user_balance)
            user_balance.save()

        return super().get(*args, **kwargs)

    def get_avalible_to_payout(self, user, user_balance):
        today = timezone.now().date()
        payments = Transaction.objects.filter(
            user=user,
            available_on__lte=today,
            type='charge'
        )
        total_payment = payments.aggregate(
            total_payment=Sum('amount')
        )['total_payment']

        payout = Transaction.objects.filter(
            user=user,
            type='payout'
        )
        total_payout = payout.aggregate(
            total_payout=Sum('amount')
        )['total_payout']

        user_balance.avalible_to_payout = (
            (total_payment if total_payment is not None else 0) +
            (total_payout if total_payout is not None else 0)
        )

    def get_avalible_to_payout_soon(self, user, user_balance):
        today = timezone.now().date()
        available_soon = Transaction.objects.filter(
            user=user,
            available_on__gt=today,
            type='charge'
        )
        total_sum = available_soon.aggregate(
            total_sum=Sum('amount')
        )['total_sum']

        user_balance.avalible_to_payout_soon = (
            total_sum if total_sum is not None else 0
        )

    def get_current_month_total(self, user, user_balance):
        current_month = timezone.now().month
        current_year = timezone.now().year

        user_balance.current_month_total = Transaction.objects.filter(
            user=user,
            created_on__month=current_month,
            created_on__year=current_year,
            type='charge'
            ).aggregate(
                current_month_total=Sum('amount'))['current_month_total'] or 0

    def get_since_last_payout(self, user, user_balance):
        last_payout = Transaction.objects.filter(
            user=user, type='payout'
        ).order_by('created_on').last()
        if last_payout:
            transactions_since_last_payout = Transaction.objects.filter(
                user=user,
                type='charge',
                created_on__gt=last_payout.created_on
            )
        else:
            transactions_since_last_payout = Transaction.objects.filter(
                user=user,
                type='charge'
            )

        user_balance.since_last_payout = (
            transactions_since_last_payout.aggregate(
                since_last_payout=Sum('amount')
                )['since_last_payout'] or 0
        )

    def get_total_volume(self, user, user_balance):
        user_balance.total_volume = Transaction.objects.filter(
            user=user,
            type='charge'
            ).aggregate(
                total_volume=Sum('amount'))['total_volume'] or 0

    def get_total_balance(self, user, user_balance):
        user_balance.total_balance = Transaction.objects.filter(
            user=user
            ).aggregate(
                total_balance=Sum('amount'))['total_balance'] or 0

    def get_total_fee(self, user, user_balance):
        user_balance.total_fee = Transaction.objects.filter(
            user=user
            ).aggregate(
                total_fee=Sum('fee'))['total_fee'] or 0

    def get_total_payout(self, user, user_balance):
        user_balance.total_payout = Transaction.objects.filter(
            user=user,
            type='payout'
            ).aggregate(total_payout=Sum('amount'))['total_payout'] or 0


class UserBalance(CustomIDModel):
    id_prefix = 'ubl'
    user = models.OneToOneField(
        User, on_delete=models.CASCADE,
        related_name='user_balance'
    )
    total_volume = models.IntegerField(default=0)
    avalible_to_payout = models.IntegerField(default=0)
    avalible_to_payout_soon = models.IntegerField(default=0)
    total_balance = models.IntegerField(default=0)
    total_payout = models.IntegerField(default=0)
    total_fee = models.IntegerField(default=0)
    since_last_payout = models.IntegerField(default=0)
    current_month_total = models.IntegerField(default=0)
    updated_on = models.DateTimeField(
        auto_now=True)
    objects = UserBalanceManager()

    def __str__(self):
        return self.user.first_name


class Transaction(CustomIDModel):
    id_prefix = 'trn'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='user_transaction'
    )
    transaction_id = models.CharField(max_length=254, unique=True)
    amount = models.IntegerField()
    currency = models.CharField(max_length=100, default="gbp")
    net = models.IntegerField(default=0)
    fee = models.IntegerField(default=0)
    tipper = models.CharField(null=True, blank=True)
    is_display = models.BooleanField(default=False)
    type = models.CharField(
        max_length=200
    )
    status = models.CharField(
        max_length=100,
        default='complete'
    )
    created_on = models.DateTimeField()
    updated_on = models.DateTimeField(auto_now=True)
    available_on = models.DateTimeField(null=True, blank=True)
    parent_id = models.CharField(max_length=254,
                                 unique=True,
                                 null=True,
                                 blank=True)
    reference_id = models.CharField(
        max_length=254, null=True, blank=True
    )
    payment_method = models.CharField(
        max_length=254, null=True, blank=True
    )
    stripe_fee = models.IntegerField(default=0, null=True)


class StripeDetail(CustomIDModel):
    id_prefix = 'std'
    user = models.OneToOneField(
        User, on_delete=models.CASCADE,
        related_name='stripe_detail'
    )
    connected_account_id = models.CharField(max_length=254)
    person_id = models.CharField(max_length=254,
                                 null=True,
                                 blank=True)
    is_identity_verified = models.BooleanField(default=False)
    is_payment_enabled = models.BooleanField(default=True)
    is_payout_enabled = models.BooleanField(default=False)

    def __str__(self):
        return self.user.email


def profile_pic_path(instance, filename):
    return f"profile_pic/{instance.user.id}/{filename}"
    # return instance.user.id


class UserProfile(CustomIDModel):
    id_prefix = 'upl'
    user = models.OneToOneField(
        User, on_delete=models.CASCADE,
        related_name='user_profile'
    )
    profile_pic = models.ImageField(null=False,  upload_to=profile_pic_path)
    qr_code = models.CharField(max_length=100, null=True, blank=True)
    created_on = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return self.user.email

    def get_profile_pic_ulr(self, request):
        domain = get_current_site(request)
        # user_profile = instance.user_profile.order_by('created_on').first()
        # user_profile = self.user_profile
        protocol = settings.PROTOCOL
        if self.profile_pic:
            return f"{protocol}://{domain}{self.profile_pic.url}"
        else:
            return None

    def delete(self, *args, **kwargs):
        if self.profile_pic:
            path = self.profile_pic.path
            try:
                if not default_storage.exists(path):
                    self.profile_pic = None
                    self.profile_pic.save()
            except:
                pass

        super().delete(*args, **kwargs)


class TemporaryUser(CustomIDModel):
    id_prefix = 'tur'
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    address = models.TextField()
    nationality = models.ForeignKey(
        master_model.Country, on_delete=models.CASCADE,
        related_name='temparary_user_country'
    )
    city = models.CharField(max_length=100)
    postal_code = models.CharField(
        max_length=10
    )
    dob = models.DateField()
    email = models.EmailField(max_length=254)
    email_verified = models.BooleanField(default=False)
    phone_country = models.ForeignKey(
        master_model.Country, on_delete=models.CASCADE,
        related_name='temparary_user_phone_country'
    )
    phone = models.CharField(max_length=25)
    phone_verified = models.BooleanField(default=False)
    password = models.CharField(max_length=128, validators=[validate_password])
    ip_address = models.CharField(max_length=200, null=True, blank=True)
    # user_otp_relation = GenericRelation('UserOtp')
    user_otp = GenericRelation(UserOtp, related_query_name='user_otp')
    email_token = GenericRelation(EmailVerificationToken,
                                  related_query_name='email_token')
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        if not self.id and self.password:
            self.password = make_password(self.password)
        super().save(*args, **kwargs)

    def user_activate(self):
        return User.objects.create(
            first_name=self.first_name,
            last_name=self.last_name,
            email=self.email,
            email_verified=self.email_verified,
            nationality=self.nationality,
            city=self.city,
            postal_code=self.postal_code,
            phone=self.phone,
            phone_country=self.phone_country,
            phone_verified=self.phone_verified,
            address=self.address,
            dob=self.dob,
            ip_address=self.ip_address,
            password=self.password,
        )


class EmailUpdate(CustomIDModel):
    id_prefix = 'ueu'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=False, blank=False,
        related_name='user_email'
    )
    new_email = models.EmailField(null=False, blank=False)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    email_token = GenericRelation(EmailVerificationToken,
                                  related_query_name='email_token')


class PhoneUpdate(CustomIDModel):
    id_prefix = 'upu'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=False,
        related_name='user_phone'
    )
    phone_country = models.ForeignKey(
        master_model.Country, on_delete=models.CASCADE, null=True, blank=True,
        related_name='phone_country'
    )
    new_phone = models.CharField(max_length=25, null=True, blank=False)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    email_token = GenericRelation(EmailVerificationToken,
                                  related_query_name='email_token')
    user_otp = GenericRelation(UserOtp, related_query_name='user_otp')

    def update_user(self):
        self.user.phone_country = self.phone_country
        self.user.phone = self.new_phone
        self.user.phone_verified = True
        self.is_active = False
        self.save()
        self.user.save()
        self.user.user_mobile_update_stripe()


class CrunchErrorDetail(CustomIDModel):
    id_prefix = 'ced'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='user_crunch_error'
    )
    email = models.EmailField(max_length=254)
    reason = models.TextField()


def pp_file_path(instance, filename):
    extantion = get_file_extension(filename)
    return f"kyc_uploads/{instance.user.id}/pp_file_{filename}"


def poa_file_path(instance, filename):
    extantion = get_file_extension(filename)
    return f"kyc_uploads/{instance.user.id}/poa_file_{filename}"


class KycFileModel(CustomIDModel):
    id_prefix = 'kfm'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='user_kyc_file'
    )
    pp_file = models.FileField(upload_to=pp_file_path)
    poa_file = models.FileField(upload_to=poa_file_path)
    created_on = models.DateTimeField(
        auto_now_add=True
    )

class DeleteAccount(CustomIDModel):
    id_prefix = 'uaa'
    user_id = models.CharField(max_length=100, unique=True)
    email = models.EmailField(max_length=254)
    phone = models.CharField(max_length=254)
    created_on = models.DateTimeField(
        auto_now_add=True
    )




# class CrunchKYCResponse(CustomIDModel):
#     user = models.ForeignKey(
#         User, on_delete=models.CASCADE,
#         related_name='user_kyc_response'
#     )
#     res_desc = models.TextField()
#     is_error = models.BooleanField(default=False)
#     created_on = models.DateTimeField(auto_now_add=True)
