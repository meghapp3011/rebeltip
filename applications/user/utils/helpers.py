# from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string

from utils.common.helpers import send_email


# def send_email(email, subject, message, html_message):
#     """
#     Send mail to the given mail address
#     """
#     from_email = 'rebeltiptest@gmail.com'
#     recipient_list = [email]
#     response = send_mail(
#         subject, message,
#         from_email, recipient_list, html_message=html_message
#     )


def send_verification_mail(instance, domain, verification_code):
    """
    Set message content for email verification after register
    and pass to send_mail function
    """
    protocol = settings.PROTOCOL
    subject = 'Email Verification'
    message = render_to_string('verification_email_template.html', {
        'verification_code': verification_code,
        'user': instance.first_name.capitalize(),
        'domain': domain,
        'protocol': protocol
    })
    send_email(
            instance.email,
            subject=subject, message=message, html_message=message
        )


def verify_new_email(instance, domain, verification_code):
    """
    Set message content for email verification after email update
    and pass to send_mail function
    """
    protocol = settings.PROTOCOL
    subject = 'Email Verification'
    user = instance.user
    message = render_to_string('new_email_verification.html', {
        'verification_code': verification_code,
        'user': f'{user.first_name.capitalize()}',
        'domain': domain,
        'protocol': protocol
    })
    send_email(
            instance.new_email, subject=subject,
            message=message, html_message=message
        )


def send_reset_password_mail(instance, domain, verification_code):
    """
    Set message content for forgot password
    and pass to send_mail function
    """
    protocol = settings.PROTOCOL
    subject = 'Password reset'
    message = render_to_string('reset_password_email_template.html', {
        'verification_code': verification_code,
        'user': f'{instance.first_name.capitalize()}',
        'domain': domain,
        'protocol': protocol
    })
    send_email(
            instance.email,
            subject=subject, message=message, html_message=message
        )


def send_promise_to_pay(email, domain, unique_key):
    protocol = settings.PROTOCOL
    subject = 'Redeem Promise to Pay'
    message = render_to_string('promise_to_pay.html', {
        'unique_key': unique_key,
        'domain': domain,
        'protocol': protocol
    })
    send_email(
        email,
        subject=subject,
        message=message,
        html_message=message
    )


def send_delete_account(instance):
    subject = 'User account delete request'
    message = render_to_string('account_delete.html', {
        'user': instance.first_name.capitalize(),
        'instance': instance
    })
    send_email(
        # email,
        email=settings.CRUNCH_EMAIL_ID,
        subject=subject,
        message=message,
        html_message=message
    )
