from django.urls import path, include
from rest_framework.routers import DefaultRouter
from applications.user.views.profile_views import UserProfileViewSet
from applications.user.views.qr_code_views import QRCodeViewSet
from .views.auth_views import AuthViewSet

router = DefaultRouter()

router.register(
    r'userprofile', UserProfileViewSet, basename='userprofile'
)

router.register(
    r'qrcode', QRCodeViewSet, basename='qrcode'
)

router.register(
    r'auth',
    AuthViewSet,
    basename='auth'
)


urlpatterns = [
    path('', include(router.urls)),
]
