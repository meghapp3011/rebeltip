from django.db import models


class ROLE_CHOICES(models.TextChoices):
    Admin = "admin", "Admin"
    User = "user", "User"


class KYC_STATUS_CHOICES(models.TextChoices):
    New = "new", "New"
    Unverified = "unverified", "Unverified"
    Verified = "verified", "Verified"
    Pending = "pending", "Pending"
    Reject = "reject", "Reject"
