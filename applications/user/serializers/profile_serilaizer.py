from rest_framework import serializers
from applications.user.models import (
    UserProfile, EmailUpdate, User, PhoneUpdate, KycFileModel
)
from applications.master import models as master_model
from django.contrib.sites.shortcuts import get_current_site
from applications.user.utils import helpers
from utils.common.send_sms import send_sms_with_twilio


class UserprofileUploadSerializer(serializers.ModelSerializer):
    profile_pic = serializers.ImageField()

    class Meta:
        model = UserProfile
        fields = ['profile_pic']

    # def update(self, instance, validated_data):
    #     existing_profile_pic = instance.profile_pic.path if instance.profile_pic else None

    #     instance.profile_pic = validated_data.get(
    #         'profile_pic', instance.profile_pic)
    #     instance.save()

    #     if existing_profile_pic and existing_profile_pic != instance.profile_pic.path:
    #         try:
    #             os.remove(existing_profile_pic)
    #         except FileNotFoundError:
    #             pass
    #     return instance

    # def to_representation(self, instance):
    #     request = self.context.get('request')
    #     domain = get_current_site(request)
    #     protocol = settings.PROTOCOL

    #     if instance.profile_pic:
    #         profile_pic_url = (
    #             instance.profile_pic.url
    #         )
    #     else:
    #         profile_pic_url = None

    #     return {'profile_pic': profile_pic_url}


class EmailUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailUpdate
        exclude = ['user']

    def validate(self, data):
        user = self.context['request'].user

        if data['new_email'] == user.email:
            raise serializers.ValidationError(
                {'email': "Email already verified"}
            )
        existing_user = User.objects.filter(
            email=data['new_email'], email_verified=True
        )
        if existing_user:
            raise serializers.ValidationError(
                {'email': "Email is already registered."}
            )

        data['user'] = user
        return data

    def save(self, **kwargs):
        instance = super(EmailUpdateSerializer, self).save(**kwargs)
        request = self.context.get('request')
        instance.user = request.user
        token = instance.email_token.create()
        domain = get_current_site(request)
        helpers.verify_new_email(instance, domain, token.key)
        # email_thread = threading.Thread(target=helpers.verify_new_email,
        #                                 args=(instance, domain, token.key))
        # email_thread.start()
        return instance


class UserProfileSerializer(serializers.ModelSerializer):
    # designation = serializers.PrimaryKeyRelatedField(
    #     queryset=master_model.Designation.objects.all()
    # )
    industry = serializers.PrimaryKeyRelatedField(
        queryset=master_model.Industry.objects.all()
    )
    profession = serializers.PrimaryKeyRelatedField(
        queryset=master_model.Profession.objects.all()
    )

    class Meta:
        model = User
        fields = [
            'id', 'first_name', 'last_name',
            'nationality', 'phone_country', 'role',
            'industry', 'profession',
            'kyc_status', 'is_active',
        ]


class UserDetailSerializer(serializers.ModelSerializer):
    profile_pic = serializers.SerializerMethodField()
    updated_email = serializers.SerializerMethodField()
    updated_phone = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id', 'first_name', 'last_name', 'email', 'email_verified',
            'phone_country', 'phone', 'phone_verified', 'kyc_status',
            'address', 'dob', 'city', 'postal_code', 'nationality', 'industry',
            'profession', 'profile_pic', 'updated_email', 'updated_phone'
        ]

    def get_updated_email(self, instance):
        user = instance
        pending_email_update = EmailUpdate.objects.filter(
            user=user).order_by('created_on').last()
        if pending_email_update and pending_email_update.is_active:
            return pending_email_update.new_email
        else:
            return None

    def get_updated_phone(self, instance):
        user = instance
        pending_phone_update = PhoneUpdate.objects.filter(
            user=user).order_by('created_on').last()
        if pending_phone_update and pending_phone_update.is_active:
            return pending_phone_update.new_phone
        else:
            return None

    def get_profile_pic(self, instance):
        request = self.context.get('request')
        # domain = get_current_site(request)
        # user_profile = instance.user_profile.order_by('created_on').first()
        # protocol = settings.PROTOCOL
        # if user_profile and user_profile.profile_pic:
        #     # return user_profile.profile_pic.url
        #     return f"{protocol}://{domain}{user_profile.profile_pic.url}"
        # else:
        #     return None
        # print(instance.user_profile)
        # user_profile = instance.user_profile
        if hasattr(instance, 'user_profile'):
            return instance.user_profile.profile_pic.url

        else:
            return None

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if data.get('updated_email') is None:
            del data['updated_email']
        if data.get('updated_phone') is None:
            del data['updated_phone']
        return data


class PhoneUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneUpdate
        fields = ['new_phone', 'phone_country']

    def validate(self, data):
        user = self.context['request'].user
        new_phone = data.get('new_phone')
        if new_phone == user.phone:
            raise serializers.ValidationError(
                {'phone': ("Phone number already verified.")}
            )
        if User.objects.filter(phone=new_phone, phone_verified=True).exists():
            raise serializers.ValidationError(
                {'phone': ("Phone number is already in use "
                           "by another verified user.")}
            )
        return data

    def save(self, **kwargs):
        user = self.context['request'].user
        instance = super(PhoneUpdateSerializer, self).save(**kwargs)
        instance.user = user
        instance.save()
        otp = instance.user_otp.create().otp
        user_std = str(instance.phone_country.std_code)
        user_mobile = str(instance.new_phone)
        user_phone = user_std + user_mobile
        send_sms_with_twilio(to_phone_number=user_phone, otp=otp)
        return instance


class KYCVerificationSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.filter(is_active=True, is_deleted=False),
        required=False)

    class Meta:
        model = KycFileModel
        fields = '__all__'

    def save(self, **kwargs):
        user = kwargs.get('user')
        return super().save(**kwargs)
