from rest_framework import serializers
from applications.user import models as user_model
from applications.master import models as master_model
from django.contrib.sites.shortcuts import get_current_site
from django.db.models import Q
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from applications.user.utils import helpers
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password
from rest_framework_simplejwt.tokens import RefreshToken
from utils.mixins import crunch_mixins
from .profile_serilaizer import UserProfileSerializer
from utils.common.send_sms import send_sms_with_twilio
from django.contrib.auth.hashers import check_password


class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model.TemporaryUser
        exclude = ['email_verified', 'phone_verified']

    def validate_phone(self, value):
        try:
            country = master_model.Country.objects.get(
                pk=self.initial_data['phone_country']
            )
        except master_model.Country.DoesNotExist:
            message = 'valid phone country is required'
            raise serializers.ValidationError(message)
        except KeyError:
            message = 'phone country required'
            raise serializers.ValidationError(message)

        unique_mob = Q(
            phone_country=country,
            phone=value,
            phone_verified=True
        )
        if user_model.User.objects.filter(unique_mob).exists():
            message = ('Mobile number has already been registered with us')
            raise serializers.ValidationError(message)
        return value

    def validate(self, data):
        email = data.get('email')
        if user_model.CrunchErrorDetail.objects.filter(email=email).exists():
            raise serializers.ValidationError({
                'detail': "Registration Error: Access Denied."
                "Your credentials do not meet our registration requirements."
            })

        duplicate_user = user_model.User.objects.filter(
            email__iexact=email, email_verified=True
        ).exists()
        if duplicate_user:
            raise serializers.ValidationError(
                {'email': 'Email already exists'}
            )
        return data

    # def validate_email(self, value):
    #     duplicate = user_model.User.objects.filter(
    #         email__iexact=value, email_verified=True)
    #     if duplicate:
    #         message = 'Email already exist'
    #         raise serializers.ValidationError(message)
    #     return value

    def save(self, **kwargs):
        instance = super(UserRegisterSerializer, self).save(**kwargs)
        token = instance.email_token.create()
        request = self.context.get('request')
        domain = get_current_site(request)
        helpers.send_verification_mail(instance, domain, token.key)
        # email_thread = threading.Thread(target=helpers.send_verification_mail,
        #                                 args=(instance, domain, token.key))
        # email_thread.start()
        return instance


class CustomIntegerField(serializers.IntegerField):
    default_error_messages = {
        'invalid': 'Please enter your OTP.',
    }


class OtpVerificationSerializer(serializers.Serializer,
                                crunch_mixins.CrunchIntegrationMixin):
    is_register = serializers.BooleanField(default=True)
    otp = CustomIntegerField()

    def validate(self, data):
        pk = self.context.get('pk')
        if data['is_register']:
            try:
                temp_user = user_model.TemporaryUser.objects.get(id=pk)
            except user_model.TemporaryUser.DoesNotExist:
                raise serializers.ValidationError(
                    {'detail': 'User does not exist'}
                )

            otp = temp_user.user_otp.order_by('created_on').last()
            if not otp or otp.otp != data['otp'] or not otp.is_valid():
                raise serializers.ValidationError({
                    'otp': "That verification code wasn't valid,"
                    "please check the code and retry"
                })

            temp_user.phone_verified = True
            temp_user.save()

            data['instance'] = temp_user

        else:
            update_data = user_model.PhoneUpdate.objects.filter(
                user__id=pk).order_by('created_on').last()

            if not update_data:
                raise serializers.ValidationError(
                    {'detail': 'User does not exist'}
                )
            otp = update_data.user_otp.order_by('created_on').last()

            if not otp or otp.otp != data['otp'] or not otp.is_valid():
                raise serializers.ValidationError({
                    'otp': "That verification code wasn't valid,"
                    "please check the code and retry"
                })

            data['instance'] = update_data

        return data

    def access(self, user):
        refresh = RefreshToken.for_user(user)
        response = {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
            # 'user': UserProfileSerializer(user).data
            **UserProfileSerializer(user).data
        }
        return response


class ResendOtpSerializer(serializers.Serializer):
    is_register = serializers.BooleanField(default=True)
    user_id = serializers.CharField(max_length=100)

    def validate(self, data):
        if data['is_register']:
            try:
                user = user_model.TemporaryUser.objects.get(
                    id=data['user_id']
                )
                user_std = str(user.phone_country.std_code)
                user_mobile = str(user.phone)
            except user_model.TemporaryUser.DoesNotExist:
                raise serializers.ValidationError(
                    {'user_id': 'User does not exist'}
                )
        else:
            try:
                user = user_model.User.objects.get(
                    id=data['user_id'],
                    is_deleted=False
                )
                user = user.user_phone.order_by('created_on').last()

                user_std = str(user.phone_country.std_code)
                user_mobile = str(user.new_phone)
            except user_model.User.DoesNotExist:
                raise serializers.ValidationError(
                    {'user_id': 'User does not exist'}
                )

        # user_std = str(user.phone_country.std_code)
        # user_mobile = str(user.phone)
        user_phone = user_std + user_mobile

        otp = user.user_otp.create().otp
        send_sms_with_twilio(to_phone_number=user_phone, otp=otp)
        return data


class EmailPhoneValidationSerializer(serializers.Serializer):
    phone_country = serializers.PrimaryKeyRelatedField(
        queryset=master_model.Country.objects.all(),
        required=False, allow_null=True)
    email_phone = serializers.CharField(max_length=100)
    is_email = serializers.BooleanField(default=True)

    def validate(self, data):
        if data['is_email']:
            if user_model.User.objects.filter(
                email=data['email_phone'],
                email_verified=True,
                is_deleted=False
            ).exists():
                raise serializers.ValidationError(
                    {'email': 'Email already exists'}
                )
        else:
            if 'phone_country' not in data or data['phone_country'] is None:
                raise serializers.ValidationError(
                    {'phone_country': 'This field is required'}
                )
            if user_model.User.objects.filter(
                phone_country=data['phone_country'],
                phone=data['email_phone'],
                phone_verified=True,
                is_deleted=False
            ).exists():
                raise serializers.ValidationError(
                    {'phone': 'Phone number already exists'}
                )
        return data


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        try:
            user = user_model.User.objects.get(
                email=attrs.get("email"),
                is_staff=False,
                is_deleted=False
            )
        except user_model.User.DoesNotExist:
            raise serializers.ValidationError(
                {'detail': ("No active user found "
                            "with the given credentials")})

        if user and not user.email_verified:
            raise serializers.ValidationError(
                {'detail': ("No active user found "
                            "with the given credentials")})

        # Extra fields in response
        data['user'] = UserProfileSerializer(user).data
        data.update(data.pop('user', {}))

        return data


class ForgotPasswordSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=100)

    def validate_email(self, value):
        try:
            user = user_model.User.objects.get(
                email__iexact=value,
                email_verified=True,
                is_active=True,
                is_deleted=False
            )
        except user_model.User.DoesNotExist:
            raise serializers.ValidationError('Email not found')

        request = self.context.get('request')
        domain = get_current_site(request)
        token = user.email_token.create()
        helpers.send_reset_password_mail(user, domain, token.key)
        # email_thread = threading.Thread(
        #     target=helpers.send_reset_password_mail,
        #     args=(user, domain, token.key)
        # )
        # email_thread.start()
        return value


class TokenValidateSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=254, required=True)

    def validate_token(self, data):
        try:
            user_token = user_model.EmailVerificationToken.objects.get(
                key=data,
                is_used=False
            )
        except user_model.EmailVerificationToken.DoesNotExist:
            raise serializers.ValidationError('Invalid token')

        if not user_token.is_valid():
            raise serializers.ValidationError('Invalid token')
        return user_token


class ResetPasswordSerializer(TokenValidateSerializer, serializers.Serializer):
    token = serializers.CharField(max_length=254, required=True)
    password = serializers.CharField(
        required=True, validators=[validate_password]
    )

    def update_password(self, validated_data):
        token = validated_data['token']
        user = token.user
        password = validated_data['password']
        if not check_password(password, user.password):
            user.password = make_password(password)
            user.save()
            token.is_used = True
            token.save()
        else:
            raise serializers.ValidationError(
                "New password must be different from the old password."
            )

        return user
