from django.contrib import admin
from applications.user import models as user_model
from django.utils import timezone


# admin.site.register(user_model.User)
admin.site.register(user_model.UserProfile)
admin.site.register(user_model.CrunchErrorDetail)
# admin.site.register(user_model.DeleteAccount)
# admin.site.register(user_model.EmailUpdate)
# admin.site.register(user_model.UserOtp)
# admin.site.register(user_model.TemporaryUser)
# admin.site.register(user_model.PhoneUpdate)
# admin.site.register(user_model.EmailVerificationToken)

class LastLoginFilter(admin.SimpleListFilter):
    title = 'Last Login'
    parameter_name = 'last_login'

    def lookups(self, request, model_admin):
        return (
            ('active', 'Active'),
            ('inactive', 'Inactive'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'active':
            sixty_days_ago = timezone.now() - timezone.timedelta(days=60)
            return queryset.filter(last_login__gte=sixty_days_ago)
        elif self.value() == 'inactive':
            sixty_days_ago = timezone.now() - timezone.timedelta(days=60)
            return queryset.filter(last_login__lt=sixty_days_ago)


@admin.register(user_model.User)
class UserModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'is_active', "is_staff", "is_superuser")
    list_filter = ("is_active", "is_staff", "is_superuser", LastLoginFilter)
    search_fields = ("id", "email")
    ordering = ('-id',)


@admin.register(user_model.TemporaryUser)
class TemporaryUserModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'email_verified',
                    'phone_country', 'phone', 'phone_verified')
    list_filter = ("email_verified", "phone_verified")
    search_fields = ("id", "email")
    ordering = ('-id',)


@admin.register(user_model.EmailVerificationToken)
class EmailVerificationTokenModelAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'object_id', 'user_email', 'created_on')
    # search_fields = ("user__email",)
    ordering = ('-created_on',)

    def user_email(self, obj):
        return obj.user.email


@admin.register(user_model.UserOtp)
class UserOtpModelAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'object_id',
                    'is_valid', 'created_on')
    # search_fields = ("user__email",)
    ordering = ('-created_on',)

    # def user_email(self, obj):
    #     print(obj.user.user_phone.email, "megha")
    #     return obj.user_ph.email


@admin.register(user_model.EmailUpdate)
class EmailUpdateModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'old_email', 'new_email', 'is_active', 'created_on')
    list_filter = ("is_active",)
    search_fields = ("id", "new_email")
    ordering = ('-created_on',)

    def old_email(self, obj):
        return obj.user.email


@admin.register(user_model.PhoneUpdate)
class PhoneUpdateModelAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'phone_country', 'new_phone', 'is_active',
                    'created_on')
    list_filter = ("is_active",)
    search_fields = ("id", "new_phone")
    ordering = ('-created_on',)

    # def old_phone_country(self, obj):
    #     return obj.user.phone_country.std_code

    # def old_phone(self, obj):
    #     return obj.user.phone


@admin.register(user_model.StripeDetail)
class StripeDetailModelAdmin(admin.ModelAdmin):
    list_display = ('user', 'connected_account_id', 'is_identity_verified')
    list_filter = ("is_identity_verified",)
    search_fields = ("user__id", "user__email")
    ordering = ('-user_id',)


@admin.register(user_model.UserBalance)
class UserBalanceAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'user', 'total_volume',
        'total_balance', 'total_payout', 'total_fee'
    )


@admin.register(user_model.Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'user', 'transaction_id',
        'amount', 'net', 'fee', 'type',
        'created_on'
    )
    ordering = ('-created_on',)
    list_filter = ("user__email", "created_on")
    search_fields = ("user__email", "transaction_id")


# @admin.register(user_model.CrunchKYCResponse)
# class CrunchKYCResponseAdmin(admin.ModelAdmin):
#     list_display = (
#         'id', 'user', 'res_desc', 'is_error'
#     )
#     search_fields = ("user__id", "user__email")


@admin.register(user_model.CrunchDetail)
class CrunchDetailAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'user', 'card_holder_ref', 'username'
    )
    search_fields = ("user__id", "user__email", "card_holder_ref")
