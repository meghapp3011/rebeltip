# views.py
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
import qrcode
from io import BytesIO
import base64


class QRCodeViewSet(viewsets.ViewSet):
    @action(detail=True, methods=['get'])
    def generate_qr_code(self, request, pk):
        if pk is not None:
            data = f"http://127.0.0.1:8000/api/user/generate_qr_code/{pk}/"
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=6,
            border=4,
        )
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        image_io = BytesIO()
        img.save(image_io, format='PNG')
        image_binary = image_io.getvalue()
        base64_data = base64.b64encode(image_binary).decode('utf-8')

        return Response({'image_data': base64_data})
