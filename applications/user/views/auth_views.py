from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from applications.user.serializers import auth_serializers

from applications.user.models import (
    EmailVerificationToken, User, CrunchErrorDetail
)
from django.http import HttpResponseRedirect

from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView
)

from rest_framework_simplejwt.serializers import (
    TokenRefreshSerializer, TokenBlacklistSerializer
)

from rest_framework_simplejwt.exceptions import TokenError

from django.conf import settings
from django.core.exceptions import ValidationError
from utils.common.send_sms import send_sms_with_twilio


class AuthViewSet(viewsets.ViewSet, TokenObtainPairView, TokenRefreshView):

    @action(detail=False, methods=['POST'])
    def register(self, request):
        """
        API Function for Registration User and send email to registered
        email address
        """
        serializer = auth_serializers.UserRegisterSerializer(
            data=request.data,
            context={'request': request})
        if serializer.is_valid():
            serializer.save()
            response = {'detail': (
                'Please check your email for a confirmation message,'
                'and click to complete your sign up.'
            )}
            return Response(response, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def email_phone_validation(self, request):
        """
        API Function for check wether verified mobile/email already exist
        """
        serializer = auth_serializers.EmailPhoneValidationSerializer(
            data=request.data
        )
        if serializer.is_valid():
            if serializer.validated_data['is_email']:
                response = {'detail': 'email is valid'}
            else:
                response = {'detail': 'phone number is valid'}
            return Response(response)

        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['GET'])
    def verify_email(self, request, pk):
        """
        API Function for validate the token and verify email
        """
        base_redirect_url = settings.REDIRECTION_URL
        try:
            token = EmailVerificationToken.objects.get(key=pk)
            model = token.content_type.model
        except EmailVerificationToken.DoesNotExist:
            message = 'Link expired.'

            return HttpResponseRedirect(
                f'{base_redirect_url}/error-page?'
                f'error_code=410&msg={message}'
            )

        if not token.is_valid():
            message = 'Link expired.'
            return HttpResponseRedirect(
                f'{base_redirect_url}/error-page?'
                f'error_code=410&msg={message}'
            )

        # if token.is_used:
        #     message = 'Email already verified'
        #     return HttpResponseRedirect(
        #         f'{base_redirect_url}/error-page?error_code=404&msg={message}'
        #     )
        base_redirect_url = settings.REDIRECTION_URL

        if model == 'temporaryuser':
            if token.user.email_verified and token.user.phone_verified:
                message = 'Email already verified.'
                return HttpResponseRedirect(
                    f'{base_redirect_url}/error-page?'
                    f'error_code=409&msg={message}'
                )
            same_user = User.objects.filter(
                email=token.user.email,
                email_verified=True
            )
            if same_user:
                message = 'Same Email already exist.'
                return HttpResponseRedirect(
                    f'{base_redirect_url}/error-page?'
                    f'error_code=409&msg={message}'
                )

            token.user.email_verified = True
            token.user.save()
            token.is_used = True
            token.save()

            # OTP creation
            otp = token.user.user_otp.create().otp

            user_std = str(token.user.phone_country.std_code)
            user_mobile = str(token.user.phone)
            user_phone = user_std + user_mobile

            send_sms_with_twilio(to_phone_number=user_phone, otp=otp)
            return HttpResponseRedirect(
                f'{base_redirect_url}/tippee/validate-mobile/{token.user.id}'
            )
        elif model == 'user':
            if token.is_used:
                message = 'Link expired.'
                return HttpResponseRedirect(
                    f'{base_redirect_url}/error-page?'
                    f'error_code=410&msg={message}'
                )

            return HttpResponseRedirect(
                f'{base_redirect_url}/tippee/reset-password/?token={pk}'
            )
        elif model == 'emailupdate':
            if token.is_used:
                message = 'Email already verified'
                return HttpResponseRedirect(
                    f'{base_redirect_url}/error-page?'
                    f'error_code=409&msg={message}'
                )
            same_user = User.objects.filter(
                email=token.user.new_email,
                email_verified=True
            )
            if same_user:
                message = 'Same Email already exist.'
                return HttpResponseRedirect(
                    f'{base_redirect_url}/error-page?'
                    f'error_code=409&msg={message}'
                )
                # return Response('Same Email already exist', status=404)
            token.user.user.email = token.user.new_email
            token.user.user.email_verified = True
            token.user.user.save()
            token.user.is_active = False
            token.user.save()
            token.is_used = True
            token.save()

            token.user.user.user_email_update_stripe()

            return HttpResponseRedirect(f'{base_redirect_url}/tippee/login/')

        message = 'Invalid link.'
        return HttpResponseRedirect(
            f'{base_redirect_url}/error-page?'
            f'error_code=410&msg={message}'
        )
        # return Response('Invalid link', status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def otp_verification(self, request):
        """
        API Function for verify user mobile with OTP.
        """
        pk = request.query_params.get('user_id')
        serializer = auth_serializers.OtpVerificationSerializer(
            data=request.data, context={'pk': pk})
        if serializer.is_valid():
            if serializer.validated_data['is_register']:
                temp_user = serializer.validated_data['instance']
                try:
                    user = temp_user.user_activate()

                    # crunch integration
                    try:
                        user.create_crunch_accound()
                        # user.check_sdd_crunch()
                        user.add_card_crunch()
                    except ValidationError as e:
                        return Response(
                            {'detail': e},
                            status=status.HTTP_423_LOCKED
                        )

                    # stripe account creation
                    try:
                        user.user_stripe_account_create()
                        user.create_user_balance()
                        response = serializer.access(user)
                        return Response(response)
                    except ValidationError as e:
                        CrunchErrorDetail.objects.create(
                            user=user,
                            email=user.email,
                            reason=str(e)
                        )
                        # user.is_active = False
                        # user.save()
                        return Response(
                            {'detail': e},
                            status=status.HTTP_423_LOCKED
                        )
                except ValidationError as e:
                    return Response(
                        {'detail': str(e)},
                        status=status.HTTP_400_BAD_REQUEST
                    )
                except Exception as e:
                    message = 'Mobile number verified already! Sign in to access your account.'
                    return Response(
                        {'detail': message},
                        status=status.HTTP_400_BAD_REQUEST
                    )
            else:
                new_mobile = serializer.validated_data['instance']
                try:
                    new_mobile.update_user()
                    response_data = {'detail': 'OTP verified successfully'}
                    return Response(response_data)
                except ValidationError as e:
                    return Response(
                        {'detail': str(e)},
                        status=status.HTTP_400_BAD_REQUEST
                        )
                except Exception as e:
                    return Response(
                        {'detail': str(e)},
                        status=status.HTTP_400_BAD_REQUEST
                    )

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def resend_otp(self, request):
        """
        API Function for resend OTP to user registered mobile number
        """
        serializer = auth_serializers.ResendOtpSerializer(data=request.data)
        if serializer.is_valid():
            response = {'detail': 'Mobile verification code resent'}
            return Response(response)

        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def forget_password(self, request):
        """
        API Function for verify user with email
        and mail reset password link
        """
        serializer = auth_serializers.ForgotPasswordSerializer(
            data=request.data,
            context={'request': request}
            )
        if serializer.is_valid():
            response = {
                'detail': 'Password reset link send to your email address'
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def reset_password(self, request):
        """
        API Function for update user password
        """
        serializer = auth_serializers.ResetPasswordSerializer(
            data=request.data
        )
        if serializer.is_valid():
            serializer.update_password(serializer.validated_data)
            response = {'detail': 'Password has been reset successfully.'}
            return Response(response)

        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    # @action(detail=False, methods=['GET'])
    # def token_validate(self, request):
    #     """
    #     API Function for validate email verification token
    #     """
    #     serializer = auth_serializers.TokenValidateSerializer(
    #         data=request.data
    #     )
    #     if serializer.is_valid():
    #         response = {'detail': 'Token valied'}
    #         return Response(response)

    #     return Response(serializer.errors,
    #                     status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['POST'])
    def login(self, request):
        """
        API Function for User Authentication with verified email
        and password and generate access and refresh token.
        """
        serializer = auth_serializers.CustomTokenObtainPairSerializer(
            data=request.data
        )
        if serializer.is_valid():
            token_data = serializer.validated_data
            return Response(token_data)
        return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)

    @action(detail=False, methods=['POST'])
    def refresh(self, request):
        """
        API Function to refresh access token with refresh token
        """
        serializer = TokenRefreshSerializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
            token_data = serializer.validated_data
            return Response(token_data)
        except TokenError as e:
            return Response({
                'refresh': str(e)}, status=status.HTTP_401_UNAUTHORIZED
            )

    @action(detail=False, methods=['POST'])
    def logout(self, request):
        """
        API Function for Invalidate User access token
        """
        serializer = TokenBlacklistSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            response = {'detail': 'Logout successfully'}
            return Response(response)
        except TokenError as e:
            return Response({
                'refresh': str(e)}, status=status.HTTP_401_UNAUTHORIZED
            )
