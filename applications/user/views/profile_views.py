from rest_framework import viewsets, status
from applications.user.serializers import profile_serilaizer
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from applications.master import models as master_model
from applications.user.models import User
from applications.user.constants import KYC_STATUS_CHOICES


class UserProfileViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        serializer = profile_serilaizer.UserDetailSerializer(
            request.user,
            context={'request': request}
            )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request):
        user = request.user
        if user.user_balance.total_balance < 1:
            user.user_stripe_account_delete()
            user.change_card_settings_crunch()
            try:
                user.user_profile.delete()
            except:
                pass
            user.delete()
            return Response(
                {"message": "User deleted successfully."},
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                {"detail": "Please payout your balance first."},
                status=status.HTTP_400_BAD_REQUEST
            )

    @action(detail=False, methods=['PUT'], url_path='update-profile')
    def update_profile(self, request):
        user = request.user
        industry_id = request.data.get('industry')
        profession_id = request.data.get('profession')

        if industry_id is not None:
            user.industry_id = industry_id
            user.profession = None

        if profession_id is not None:
            user.profession_id = profession_id

        serializer = profile_serilaizer.UserProfileSerializer(
            user, data=request.data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['PUT'], url_path='update-image')
    def profile_pic_update(self, request, *args, **kwargs):
        try:
            user_profile = request.user.user_profile
            serializer = profile_serilaizer.UserprofileUploadSerializer(
                user_profile,
                data=request.data,
                context={'request': request}
            )
        except:
            # user_profile = None
            serializer = profile_serilaizer.UserprofileUploadSerializer(
                data=request.data,
                context={'request': request}
            )

        if serializer.is_valid():
            # if not user_profile:
            serializer.validated_data['user'] = request.user

            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)

    @action(detail=False, methods=['POST'], url_path='update-email')
    def email_update(self, request, *args, **kwargs):
        serializer = profile_serilaizer.EmailUpdateSerializer(
            data=request.data, context={'request': request}
        )
        if serializer.is_valid():
            serializer.save()
            response_data = {
                "detail": "Verification mail has been sent to your email."
            }
            return Response(response_data, status=status.HTTP_200_OK)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

    @action(detail=False, methods=['POST'], url_path='update-phone')
    def phone_update(self, request, *args, **kwargs):
        phone_country = request.data.get('phone_country')
        try:
            phone_country = master_model.Country.objects.get(pk=phone_country)
        except master_model.Country.DoesNotExist:
            return Response(
                {"detail": "Invalid country."},
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = profile_serilaizer.PhoneUpdateSerializer(
            data=request.data, context={'request': request}
        )
        if serializer.is_valid():
            serializer.save()
            response_data = {"detail": "OTP sent for verification."}
            return Response(response_data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

    @action(detail=False, methods=['GET'])
    def kyc_status(self, request):
        # user_id = request.query_params.get('user_id')
        user = request.user
        try:
            # user = User.objects.get(id=user_id)
            # kyc_status = user.kyc_status
            kyc_status = user.kyc_status
            print('kyc_status : ', kyc_status)
            return Response({'kyc_status': kyc_status})
        except User.DoesNotExist:
            return Response(
                {'error': 'User not found'}, status=status.HTTP_404_NOT_FOUND
            )
        except Exception as e:
            return Response(
                {'error': str(e)}, status=status.HTTP_400_BAD_REQUEST
            )

    @action(detail=False, methods=['POST'])
    def kyc_data(self, request):

        user = request.user
        serializer = profile_serilaizer.KYCVerificationSerializer(
            data=request.data
        )

        if serializer.is_valid():
            serializer.save(user=user)
            data = user.kyc_verification_crunch()
            if data['actionCode'] == "000":
                if (
                    data['resultIdScan']['cardholderStatus'] ==
                    'KYC Pending Verification'
                ):
                    user.kyc_status = KYC_STATUS_CHOICES.Pending
                    user.save()
            else:
                user.user_kyc_file.all().delete()
                return Response(
                    {'detail': data['actionCodeDescription']},
                    status=status.HTTP_400_BAD_REQUEST
                )
            user.user_kyc_file.all().delete()
            response = {
                'detail': 'Documents received.'
                'Review underway. Await notification.'
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # @action(detail=False, methods=['GET'])
    # def balance(self, request):
    #     user = request.user
    #     balance = user.user_connect_account_balance_stripe()
    #     return Response(balance)

    # @action(detail=False, methods=['GET'])
    # def payouts(self, request):
    #     user = request.user
    #     payout = user.connect_account_payout_list_stripe()
    #     return Response(payout)

    # @action(detail=False, methods=['GET'])
    # def transations(self, request):
    #     user = request.user
    #     transations = user.user_connect_account_balance_transation_stripe()
    #     return Response(transations)

    # @action(detail=False, methods=['POST'])
    # def payout(self, request):
    #     serializer = profile_serilaizer.PayoutSerializer(data=request.data)
    #     user = request.user
    #     if serializer.is_valid():
    #         payout = user.user_connect_account_payout_stripe(
    #             serializer.validated_data['amount']
    #         )
    #         user.create_stripe_object_to_user(payout)
    #         return Response(payout)
        # return Response(
        #     serializer.errors, status=status.HTTP_400_BAD_REQUEST
        # )

    @action(detail=False, methods=['GET'])
    def date_joined(self, request):
        user = request.user
        try:
            created_on = user.date_joined.date()
            print(created_on, "megha")
            return Response({'created_on': created_on})
        except User.DoesNotExist:
            return Response(
                {'error': 'User not found'}, status=status.HTTP_404_NOT_FOUND
            )
        except Exception as e:
            return Response(
                {'error': str(e)}, status=status.HTTP_400_BAD_REQUEST
            )
