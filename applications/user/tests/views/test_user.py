from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status

from applications.master.tests.factories import master_factory


class UserTestCase(APITestCase):
    def setUp(self):

        self.country = master_factory.CountryFactory()

        self.data = {
                "first_name": "test",
                "last_name": "one",
                "address": "test address",
                "nationality": self.country.pk,
                "dob": "1993-12-26",
                "phone_country": self.country.pk,
                "phone": "7591942362",
                "email": "shammas.nasar@panasatech.com",
                "password": "sdfadsasd156"
                }

    def test_register(self):
        url = reverse('auth-register')

        # success
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    #     # Duplicate mobile number
    #     user = user_factory.UserFactory(
    #         mobile_country = self.country,
    #     )
    #     self.data['mobile_number'] = user.mobile_number
    #     response = self.client.post(url, self.data, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
