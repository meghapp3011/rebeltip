from django.db.models.signals import pre_delete, pre_save, post_save
from django.dispatch import receiver
from applications.user.models import KycFileModel, UserProfile, User, DeleteAccount
from applications.user.utils import helpers
import boto3
from django.conf import settings

s3 = boto3.client('s3')


def delete_existing_files(folder_prefix):
    objects_to_delete = s3.list_objects_v2(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Prefix=folder_prefix)

    if 'Contents' in objects_to_delete:
        object_keys = [obj['Key'] for obj in objects_to_delete['Contents']]

        for key in object_keys:
            s3.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=key)
            print('deleted')

# @receiver(pre_delete, sender=KycFileModel)
# def delete_kyc_file_on_delete(sender, instance, **kwargs):
#     if os.path.isfile(instance.pp_file.path):
#         os.remove(instance.pp_file.path)

#     if os.path.isfile(instance.poa_file.path):
#         os.remove(instance.poa_file.path)


# @receiver(pre_delete, sender=UserProfile)
# def delete_user_profile_on_delete(sender, instance, **kwargs):
#     if instance.profile_pic:
#         file_path = instance.profile_pic.path
#         if os.path.isfile(file_path):
#             os.remove(file_path)

#         # Check if the folder is empty and delete it
#         folder_path = os.path.dirname(file_path)
#         if not os.listdir(folder_path):
#             os.rmdir(folder_path)


@receiver(pre_delete, sender=User)
def delete_user_account(sender, instance, **kwargs):
    helpers.send_delete_account(instance)
    # DeleteAccount.objects.create(
    #     user_id = instance.id,
    #     email = instance.email,
    #     phone = instance.phone
    # )

    # email_thread = threading.Thread(
    #             target=helpers.send_delete_account,
    #             args=(instance, instance.email)
    #         )
    # email_thread.start()


@receiver(pre_save, sender=UserProfile)
def delete_previous_file(sender, instance, **kwargs):
    if instance.profile_pic:
        id = instance.user.id
        folder_prefix = f'media/profile_pic/{id}/'
        delete_existing_files(folder_prefix)


@receiver(pre_delete, sender=UserProfile)
def delete_user_profile_files(sender, instance, **kwargs):
    if instance.profile_pic:
        id = instance.user.id
        folder_prefix = f'media/profile_pic/{id}/'
        delete_existing_files(folder_prefix)


# @receiver(pre_delete, sender=KycFileModel)
# def delete_kyc_file(sender, instance, **kwargs):
#     if instance.pp_file:
#         path = instance.pp_file.name
#         try:
#             s3.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=path)
#         except Exception as e:
#             print(f"An error occurred while deleting 'pp_file': {str(e)}")

#     if instance.poa_file:
#         path = instance.poa_file.name
#         try:
#             s3.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=path)
#         except Exception as e:
#             print(f"An error occurred while deleting 'poa_file': {str(e)}")


# @receiver(post_save, sender=KycFileModel)
# def delete_uploaded_files(sender, instance, created, **kwargs):
#     if created:
#         instance._old_pp_file = instance.pp_file.name if instance.pp_file else None
#         instance._old_poa_file = instance.poa_file.name if instance.poa_file else None

# @receiver(pre_delete, sender=KycFileModel)
# def delete_kyc_file(sender, instance, **kwargs):
#     # Check if the instance has associated files and delete them
#     if hasattr(instance, '_old_pp_file') and instance._old_pp_file:
#         try:
#             s3.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=instance._old_pp_file)
#         except Exception as e:
#             pass

#     if hasattr(instance, '_old_poa_file') and instance._old_poa_file:
#         try:
#             s3.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=instance._old_poa_file)
#         except Exception as e:
#             pass


@receiver(pre_delete, sender=KycFileModel)
def delete_kyc_file(sender, instance, **kwargs):
    id = instance.user.id
    folder_prefix = f'media/kyc_uploads/{id}/'
    delete_existing_files(folder_prefix)
