from rest_framework import serializers
from applications.user import models as user_models
from . import models as account_models
from django.conf import settings
from applications.user.utils import helpers
from applications.user.constants import KYC_STATUS_CHOICES


DOCUMENT_TYPES = {
    "passport": {"name": "passport", "no_of_sides": 2},
    "driving_license": {"name": "driving_license", "no_of_sides": 2},
    "resident_permit": {"name": "resident_permit", "no_of_sides": 2},
    "citizen_card": {"name": "citizen_card", "no_of_sides": 2},
    "electoral_id": {"name": "electoral_id", "no_of_sides": 1},
    "other_document": {"name": "other_document", "no_of_sides": 1}
}


class PromiseToPaySerializer(serializers.ModelSerializer):

    class Meta:
        model = account_models.PromiseToPay
        fields = '__all__'


class RedeemPromiseSerializer(serializers.Serializer):
    unique_key = serializers.CharField(max_length=254)

    def validate_unique_key(self, value):
        try:
            object = account_models.PromiseToPay.objects.get(
                unique_key=value
            )
            if object.is_redeemed:
                message = 'Code already used. '
                raise serializers.ValidationError(message)
        except account_models.PromiseToPay.DoesNotExist:
            message = 'Code entered not valid .'
            raise serializers.ValidationError(message)
        return object

    def send_link_mail(self, email, unique_key):
        domain = settings.REDIRECTION_URL
        helpers.send_promise_to_pay(email, domain, unique_key)
        # email_thread = threading.Thread(
        #         target=helpers.send_promise_to_pay,
        #         args=(email, domain, unique_key)
        #         )
        # email_thread.start()


class PromiseToTipSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    industry = serializers.SerializerMethodField()
    profession = serializers.SerializerMethodField()

    class Meta:
        model = account_models.PromiseToPay
        fields = ['username', 'profession', 'industry', 'amount']

    def get_username(self, instance):
        username = f"{instance.tippee.first_name} {instance.tippee.last_name}"
        return username

    def get_industry(self, instance):
        industry = instance.tippee.industry.name
        return industry

    def get_profession(self, instance):
        profession = instance.tippee.profession.name
        return profession


class AddTipSerializer(serializers.ModelSerializer):

    class Meta:
        model = account_models.PromiseToPay
        fields = ['amount', 'tipper', 'show_name', 'reason']


class BankAccountSerializer(serializers.Serializer):
    account_holder_name = serializers.CharField(max_length=100)
    routing_number = serializers.CharField(max_length=100)
    account_number = serializers.CharField(max_length=100)


class KYCVerificationSerializer(serializers.Serializer):
    DOCUMENT_CHOICES = [
        (key, value['name']) for key, value in DOCUMENT_TYPES.items()
    ]
    document_type = serializers.ChoiceField(choices=DOCUMENT_CHOICES)
    front_side = serializers.ImageField(required=True)
    back_side = serializers.ImageField(required=False)

    def validate(self, data):
        document_type = data.get('document_type')
        back_side = data.get('back_side')

        document_type_details = DOCUMENT_TYPES.get(document_type)

        if document_type_details['no_of_sides'] == 2 and not back_side:
            raise serializers.ValidationError({
                'detail': "Both sides of the document are required."
            })

        return data


class NewUserBalanceSerializer(serializers.ModelSerializer):
    total_volume = serializers.SerializerMethodField()
    avalible_to_payout = serializers.SerializerMethodField()
    avalible_to_payout_soon = serializers.SerializerMethodField()
    total_balance = serializers.SerializerMethodField()
    total_payout = serializers.SerializerMethodField()
    total_fee = serializers.SerializerMethodField()
    since_last_payout = serializers.SerializerMethodField()
    current_month_total = serializers.SerializerMethodField()

    class Meta:
        model = user_models.UserBalance
        fields = '__all__'

    def get_current_month_total(self, obj):
        return obj.current_month_total/100

    def get_total_volume(self, obj):
        return obj.total_volume/100

    def get_total_balance(self, obj):
        return obj.total_balance/100

    def get_total_payout(self, obj):
        return obj.total_payout/100

    def get_total_fee(self, obj):
        return obj.total_fee/100

    def get_since_last_payout(self, obj):
        return obj.since_last_payout/100

    def get_avalible_to_payout(self, obj):
        return obj.avalible_to_payout/100

    def get_avalible_to_payout_soon(self, obj):
        return obj.avalible_to_payout_soon/100


class TransactionSerializer(serializers.ModelSerializer):
    amount = serializers.SerializerMethodField()
    net = serializers.SerializerMethodField()
    fee = serializers.SerializerMethodField()

    class Meta:
        model = user_models.Transaction
        # fields = '__all__'
        exclude = ['stripe_fee']

    def get_amount(self, obj):
        return '{:.2f}'.format(obj.amount/100)

    def get_net(self, obj):
        return '{:.2f}'.format(obj.net/100)

    def get_fee(self, obj):
        return '{:.2f}'.format(obj.fee/100)


class BankDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = account_models.BankAccountDetail
        fields = '__all__'


class NewPayoutSerializer(serializers.Serializer):
    amount = serializers.FloatField()

    # def can_checkout(self, user, amount):
    #     self.have_bank_account(user)
    #     self.have_balance(user, amount)
    #     self.kyc_verified(user)
    #     return True

    def kyc_verified(self, user):
        if user.kyc_status == KYC_STATUS_CHOICES.New:
            is_ekyc_approved = user.ekyc_verification_crunch()
            if is_ekyc_approved:
                return True, ''
        elif user.kyc_status == KYC_STATUS_CHOICES.Verified:
            return True, ''
        elif user.kyc_status == KYC_STATUS_CHOICES.Pending:
            return False, 'pending'
        elif user.kyc_status == KYC_STATUS_CHOICES.Unverified:
            return False, 'unverified'
        # raise serializers.ValidationError(
        #     {'detail': (
        #         'We need to perform security checks to allow further loads '
        #         'into your account. Please upload proof of address and '
        #         'proof of identification.'
        #     )}
        #     )

    def have_bank_account(self, user):
        try:
            user.bank_account_details
        except Exception:
            raise serializers.ValidationError({
                'detail': "Bank account not found."
            })
        return True

    def have_balance(self, user, amount):
        payoutable_balance = user.user_balance.avalible_to_payout

        if payoutable_balance == 0 or payoutable_balance < amount:
            raise serializers.ValidationError({
                'detail': "There is no insufficient balance."
            })
        if amount < 200:
            raise serializers.ValidationError({
                'detail':
                "The cash out amount must be greater than or equal to 2£."
            })
        return True


class DownloadTransactionSerializer(serializers.Serializer):
    month = serializers.CharField()
    year = serializers.IntegerField()

    class Meta:
        model = user_models.Transaction
        fields = [
            'id', 'amount', 'currency',
            'net', 'fee', 'type', 'status'
            ]
