from django.contrib import admin
from . import models as accoutn_models


@admin.register(accoutn_models.PromiseToPay)
class PromiseToPayAdmin(admin.ModelAdmin):
    list_display = ('id', 'tipper')
    ordering = ('tipper',)


# @admin.register(accoutn_models.Transaction)
# class TransactionAdmin(admin.ModelAdmin):
#     list_display = (
#         'id', 'user', 'transaction_id',
#         'amount', 'net', 'fee', 'type',
#         'created_on'
#     )
#     ordering = ('-created_on',)
#     list_filter = ("user", "created_on")
#     search_fields = ("user__id", "user__email", "transaction_id")


@admin.register(accoutn_models.BankAccountDetail)
class BankAccountDetailAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'user', 'bank_account_id',
        'bank_name', 'last4', 'is_verified'
    )


@admin.register(accoutn_models.CrunchResponse)
class CrunchResponseAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'service', 'response', 'created_on'
    )
    ordering = ('-created_on',)
    list_filter = ("service", "created_on")


@admin.register(accoutn_models.CheckoutSessions)
class CheckoutSessionsAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'user', 'checkout_session_id', 'amount', 'status'
    )
    ordering = ('-created_on',)
    list_filter = ("status",)


@admin.register(accoutn_models.Payout)
class PayoutAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'user', 'payout_id', 'amount', 'status'
    )
    ordering = ('-created_on',)
    list_filter = ("status",)
