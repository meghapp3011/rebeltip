from django.urls import path, include
from rest_framework.routers import DefaultRouter

# from . import views as account_view

from .views import webhook_views
from .views import account_views


router = DefaultRouter()

router_without_trailing_slash = DefaultRouter(trailing_slash=False)


# router.register(
#     r'verification', account_views.StripeAccountVerificationView,
#     basename='verification'
# )

router.register(
    r'bank_account', account_views.BankAccountViewSet,
    basename='bank_account'
)

router.register(
    r'promise-to-pay', account_views.PromiseToPayViewSet,
    basename='promise_to_pay'
)

router.register(
    r'stripe_webhook', webhook_views.StripeWebhookViewSet,
    basename='stripe_webhook'
)
router_without_trailing_slash.register(
    r'crunch_webhook', webhook_views.CrunchWebhookViewSet,
    basename='crunch_webhook'
)

urlpatterns = [
    path('', include(router.urls)),
    path('', include(router_without_trailing_slash.urls)),
]
