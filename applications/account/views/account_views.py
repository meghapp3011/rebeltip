from rest_framework.response import Response
from rest_framework import viewsets, generics, status
from rest_framework.decorators import action
from applications.account import models as account_models
from rest_framework.permissions import IsAuthenticated
from applications.account import serializer as account_serializer
from applications.user import models as user_model
from applications.account.utils import helpers


class BankAccountViewSet(generics.ListAPIView, viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    @action(detail=False, methods=['GET'])
    def details(self, request):
        queryset = account_models.BankAccountDetail.objects.filter(
            user=request.user
        ).order_by('created_on').last()
        if queryset is None:
            return Response({})
        serializer = account_serializer.BankDetailSerializer(queryset)
        return Response(serializer.data)

    @action(detail=False, methods=['POST'])
    def add(self, request):
        user = request.user
        serializer = account_serializer.BankAccountSerializer(
            data=request.data
        )
        if serializer.is_valid():
            try:
                external_account = user.add_user_bank_account_stripe(
                    serializer.validated_data
                )
                current_bank_account = account_models.BankAccountDetail.objects.filter(
                    user=user
                )
                if current_bank_account.exists():
                    user.delete_user_bank_account_stripe()
                    current_bank_account.delete()

                bank_details = account_models.BankAccountDetail.objects.create(
                    user=user,
                    bank_account_id=external_account['id'],
                    bank_name=external_account['bank_name'],
                    account_holder_name=external_account['account_holder_name'],
                    last4=external_account['last4']
                )
                serialized_data = account_serializer.BankDetailSerializer(
                    bank_details
                )
                return Response(serialized_data.data)
            except Exception as e:
                message = "Please ensure your credentials are correct."
                response = {'detail': message}
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['GET'])
    def balance(self, request):
        try:
            queryset = user_model.UserBalance.objects.get(user=request.user)

        except user_model.UserBalance.DoesNotExist:
            response = {'detail': 'Balance details does not exist'}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        serializer = account_serializer.NewUserBalanceSerializer(queryset)
        return Response(serializer.data)

    @action(detail=False, methods=['GET'])
    def transaction(self, request):
        queryset = user_model.Transaction.objects.filter(
            user=request.user
        ).order_by('-created_on')
        paginated_queryset = self.paginate_queryset(queryset)
        serializer = account_serializer.TransactionSerializer(
            paginated_queryset, many=True
        )
        return self.get_paginated_response(serializer.data)

    @action(detail=False, methods=['POST'])
    def payout(self, request):
        serializer = account_serializer.NewPayoutSerializer(
            data=request.data
        )
        user = request.user

        if serializer.is_valid():
            amount = int(serializer.validated_data['amount']*100)

            # serializer.can_checkout(user, amount)

            serializer.have_bank_account(user)
            serializer.have_balance(user, amount)

            user_kyc_verified, state = serializer.kyc_verified(user)

            if not user_kyc_verified:
                if state == 'unverified':
                    message = (
                        'We need to perform security checks to allow further loads '
                        'into your account. Please upload proof of address and '
                        'proof of identification.'
                    )
                    response = {"detail": message}
                    return Response(
                        response, status=status.HTTP_417_EXPECTATION_FAILED
                    )
                elif state == 'pending':
                    message = 'KYC verification is currently in progress. Thank you for the patience.'
                    response = {"detail": message}
                    return Response(
                        response, status=status.HTTP_400_BAD_REQUEST
                    )
                else:
                    message = ''
                    response = {"detail": message}
                    return Response(
                        response, status=status.HTTP_400_BAD_REQUEST
                    )

                response = {"detail": message}
                return Response(
                    response, status=status.HTTP_417_EXPECTATION_FAILED
                )

            # try:
            #     serializer.kyc_verified(user)
            # except serializers.ValidationError as e:

            after_charge = int(helpers.payoutable_amount(96, amount))

            user.user_transfer_create_stripe(after_charge)

            payout = user.user_connect_account_payout_stripe(after_charge)
            fee = amount - after_charge
            user.payout.create(
                user=user,
                payout_id=payout.id,
                amount=-amount,
                net=-after_charge,
                fee=fee
            )
            fee_display = fee/100
            response = {
                'detail':
                f"We are processing your cash withdrawal of "
                f"£{amount/100:.2f} as requested. "
                f"Please note that a service charge of "
                f"£{fee_display:.2f} will be applied to your transaction. "
                f"The balance that will be credited to your bank account "
                f"after the service charge deduction will be "
                f"£{after_charge/100:.2f}."
            }
            return Response(response, status=status.HTTP_200_OK)
        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST
        )

    @action(detail=False, methods=['GET'])
    def download_transactions(self, request):
        month = request.query_params.get('month')
        year = request.query_params.get('year')
        serializer = account_serializer.DownloadTransactionSerializer(
            data={'month': month, 'year': year}
        )
        if serializer.is_valid():
            user = request.user
            response = helpers.transaction_download(
                serializer.validated_data, user, request
            )
            return response
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )


class PromiseToPayViewSet(viewsets.ViewSet):
    serializer_class = account_serializer.PromiseToPaySerializer

    @action(detail=False, methods=['POST'], url_path='redeem',
            permission_classes=[IsAuthenticated])
    def redeem_promise_to_pay(self, request):

        serializer = account_serializer.RedeemPromiseSerializer(
            data=request.data
        )
        if serializer.is_valid():
            data = serializer.validated_data['unique_key']
            data.tippee = request.user
            data.save()
            serializer.send_link_mail(data.tipper, data.unique_key)
            response = {
                'detail': 'Email sent to tipper for redemption'
            }
            return Response(response)
        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST
        )
