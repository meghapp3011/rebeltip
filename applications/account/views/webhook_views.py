from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
import stripe
from applications.account import models as account_models
from django.conf import settings
from utils.mixins.stripe_mixins import StripeIntegrationMixin
from datetime import datetime

from applications.notification.utils.helpers import create_notification
from applications.notification.constants import NotificationType

from applications.user.models import User
from applications.user.constants import KYC_STATUS_CHOICES


class StripeWebhookViewSet(viewsets.ViewSet, StripeIntegrationMixin):

    stripe.api_key = settings.STRIPE_SECRET_KEY

    # @action(detail=False, methods=['POST'])
    # def connected_account_update(self, request, format=None):

    #     webhook_secret = 'whsec_RBK6IW0ZwF7tYuXBdWYSZxLx8dADu2A2'

    #     payload = request.body
    #     sig_header = request.headers.get('Stripe-Signature')

    #     try:
    #         event = stripe.Webhook.construct_event(
    #             payload, sig_header, webhook_secret
    #         )
    #     except ValueError as e:
    #         print('e : ', e)
    #         return Response(e, status=status.HTTP_400_BAD_REQUEST)
    #     except stripe.error.SignatureVerificationError as e:
    #         print('e2 : ', e)
    #         return Response(e, status=status.HTTP_400_BAD_REQUEST)

    #     # Handle the account.updated event specifically
    #     if event['type'] == 'account.updated':
    #         data = event['data']['object']

    #         print('data : ', data)

    #     return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['POST'])
    def checkout_session_update(self, request):

        webhook_secret = settings.STRIPE_CHECKOUT_SECRET

        payload = request.body
        sig_header = request.headers.get('Stripe-Signature')

        try:
            event = stripe.Webhook.construct_event(
                payload, sig_header, webhook_secret
            )
        except ValueError as e:
            print('e : ', e)
            return Response(e, status=status.HTTP_400_BAD_REQUEST)
        except stripe.error.SignatureVerificationError as e:
            print('e2 : ', e)
            return Response(e, status=status.HTTP_400_BAD_REQUEST)

        try:
            session = account_models.CheckoutSessions.objects.get(
                checkout_session_id=event['data']['object']['id'],
            )

        except Exception:
            message = 'No object'
            return Response(message, status=status.HTTP_400_BAD_REQUEST)

        payment_status = event['data']['object']['status']

        if payment_status == 'expired':
            session.status = event['data']['object']['status']
            session.save()
            return Response(status=status.HTTP_200_OK)

        elif payment_status == 'complete' and not session.transaction_added:
            total_amount = event['data']['object']['amount_total']
            session.status = payment_status
            # save payment method
            payment_method_types = event['data']['object']['payment_method_types']
            payment_method = payment_method_types[0]
            session.payment_method = payment_method



            # crunch wallet load
            session.user.load_wallet_crunch(total_amount/100, session.tipper)

            transactions = self.stripe_connect_account_balance_transation(10)

            for i in transactions:
                if i['amount'] == total_amount and i['type'] == 'charge':
                    session.transaction_id = i['id']
                    session.transaction_added = True
                    session.stripe_fee = i['fee']
                    session.available_on = datetime.utcfromtimestamp(
                        i['available_on']
                    )
                    try:
                        session.save()
                        break
                    except Exception:
                        pass

        return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['POST'])
    def payout_update(self, request):

        webhook_secret = settings.STRIPE_PAYOUT_SECRET

        payload = request.body
        sig_header = request.headers.get('Stripe-Signature')

        try:
            event = stripe.Webhook.construct_event(
                payload, sig_header, webhook_secret
            )
        except ValueError as e:
            print('e : ', e)
            return Response(e, status=status.HTTP_400_BAD_REQUEST)
        except stripe.error.SignatureVerificationError as e:
            print('e2 : ', e)
            return Response(e, status=status.HTTP_400_BAD_REQUEST)
        try:
            payout = account_models.Payout.objects.get(
                payout_id=event['data']['object']['id']
            )
        except Exception:
            pass

        payout.status = event['data']['object']['status']
        payout.transaction_id = event['data']['object']['balance_transaction']
        try:
            payout.save()
        except Exception:
            pass

        if event['data']['object']['status'] == 'failed':
            user = payout.user
            header = 'Payout failed'
            description = event['data']['object']['failure_message']
            notification_type = NotificationType.PAYOUT_FAILED
            create_notification(
                user,
                header,
                description,
                notification_type
            )
        elif event['data']['object']['status'] == 'paid':
            amount = abs(payout.amount)/100
            payout.user.unload_wallet_crunch(amount, payout.user.first_name)

        return Response(status=status.HTTP_200_OK)


class CrunchWebhookViewSet(viewsets.ViewSet):
    @action(detail=False, methods=['POST'])
    def statuschange(self, request):
        data = request.data

        account_models.CrunchResponse.objects.create(
            service='webhook',
            response=data
        )

        try:
            user = User.objects.get(crunch_detail__card_holder_ref=data['CardholderRef'])
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if user.kyc_status != KYC_STATUS_CHOICES.Verified:
            if data['Status'] == "Passed KYC":

                user.kyc_status = KYC_STATUS_CHOICES.Verified

            else:
                user.kyc_status = KYC_STATUS_CHOICES.Unverified

                header = 'KYC verification failed'
                description = ('We need to perform security checks to allow '
                               'further loads into your account. Please upload '
                               'proof of address and proof of identification.')
                notification_type = NotificationType.KYC_ALERT
                create_notification(
                    user,
                    header,
                    description,
                    notification_type
                )

        user.save()
        return Response(status=status.HTTP_200_OK)
