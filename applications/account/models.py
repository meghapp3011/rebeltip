from django.db import models
from applications.user.models import User
import string
import random
import re
from utils.common.custom_id_field import CustomIDModel


class BankAccountDetail(CustomIDModel):
    id_prefix = 'bnk'
    user = models.OneToOneField(
        User, on_delete=models.CASCADE,
        related_name='bank_account_details'
    )
    bank_account_id = models.CharField(max_length=254)
    bank_name = models.CharField(max_length=254)
    account_holder_name = models.CharField(max_length=254)
    last4 = models.CharField(max_length=10)
    is_verified = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)


class CheckoutSessions(CustomIDModel):
    id_prefix = 'cos'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='checkout_sessions'
    )
    checkout_session_id = models.CharField(max_length=254, unique=True)
    reference_id = models.CharField(
        max_length=254, null=True, blank=True
    )
    amount = models.IntegerField()
    net = models.IntegerField(null=True, blank=True)
    fee = models.IntegerField(null=True, blank=True, default=0)
    currency = models.CharField(max_length=100, default='gbp')
    tipper = models.CharField(null=True, blank=True)
    is_display = models.BooleanField(default=False)
    status = models.CharField(default='pending')
    transaction_id = models.CharField(
        max_length=254, null=True, blank=True, unique=True
    )
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    available_on = models.DateTimeField(null=True, blank=True)
    transaction_added = models.BooleanField(default=False)
    payment_method = models.CharField(
        max_length=254, null=True, blank=True
    )
    stripe_fee = models.IntegerField(default=0, null=True)
   

class Payout(CustomIDModel):
    id_prefix = 'pyt'
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='payout'
    )
    payout_id = models.CharField(max_length=254, unique=True)
    currency = models.CharField(max_length=100, default='gbp')
    amount = models.IntegerField()
    net = models.IntegerField(null=True, blank=True)
    fee = models.IntegerField(null=True, blank=True, default=0)
    status = models.CharField(default='pending')
    transaction_id = models.CharField(
        max_length=254, null=True, blank=True
    )
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class PromiseToPay(CustomIDModel):
    id_prefix = 'ptp'
    tipper = models.EmailField(max_length=100)
    tippee = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='user_promise_to_pay', null=True
    )
    amount = models.FloatField()
    unique_key = models.CharField(
        max_length=100, null=True, blank=True, unique=True
    )
    created_on = models.DateTimeField(auto_now_add=True)
    is_redeemed = models.BooleanField(default=False)
    show_name = models.BooleanField(default=False)
    reason = models.TextField(null=True, blank=True)

    def generate_unique_key(self):
        email_first_name = self.tipper.split('@')[0]
        email_first_name = re.sub(r'[^\w\s]', '', email_first_name)
        random_chars = ''.join(random.choices(
            string.ascii_letters + string.digits, k=10
        ))
        unique_key = f"{email_first_name}.{random_chars}"
        return unique_key

    def save(self, *args, **kwargs):
        if not self.unique_key:
            self.unique_key = self.generate_unique_key()
        super().save(*args, **kwargs)


class CrunchResponse(CustomIDModel):
    id_prefix = 'crs'
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        null=True, blank=True,
        related_name='user_crunch_response'
    )
    service = models.CharField(max_length=254, null=True, blank=True)
    response = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
