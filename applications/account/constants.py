from django.db import models


class Type(models.TextChoices):
    CHARGE = "Charge", "charge"
    PAYOUT = "Payout", "payout"


class TransactionStatus(models.TextChoices):
    SUCCESS = "Success", "success"
    PENDING = "Pending", "pending"
    UNPAID = "Unpaid", "unpaid"
