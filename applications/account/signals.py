from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import CheckoutSessions, Payout
from applications.user.models import Transaction, UserBalance
from applications.account.models import PromiseToPay
from applications.account.utils import helpers


@receiver(post_save, sender=Payout)
def send_payout(sender, instance, **kwargs):
    if instance.status == "paid":
        email = instance.user.email
        helpers.send_payout_mail(instance, email)
        # email_thread = threading.Thread(
        #         target=helpers.send_payout_mail,
        #         args=(instance, email)
        #     )
        # email_thread.start()


@receiver(post_save, sender=Transaction)
def update_account_balance(sender, instance, created, **kwargs):
    user = instance.user
    user_balance = user.user_balance

    # current_month = timezone.now().month
    # current_year = timezone.now().year
    # today = timezone.now().date()

    if created:
        UserBalance.objects.get_avalible_to_payout(user, user_balance)
        UserBalance.objects.get_avalible_to_payout(user, user_balance)
        UserBalance.objects.get_avalible_to_payout_soon(user, user_balance)
        UserBalance.objects.get_current_month_total(user, user_balance)
        UserBalance.objects.get_since_last_payout(user, user_balance)
        UserBalance.objects.get_total_volume(user, user_balance)
        UserBalance.objects.get_total_balance(user, user_balance)
        UserBalance.objects.get_total_payout(user, user_balance)
        UserBalance.objects.get_total_fee(user, user_balance)
        user_balance.save()

    else:
        pass

    user_balance.save()


@receiver(post_save, sender=CheckoutSessions)
def update_transaction_payment(sender, instance, created, **kwargs):
    if instance.status == 'complete':
        instance.user.user_transaction.create(
            transaction_id=instance.transaction_id,
            amount=instance.amount,
            currency=instance.currency,
            net=instance.net,
            fee=instance.fee,
            tipper=instance.tipper,
            is_display=instance.is_display,
            type='charge',
            created_on=instance.created_on,
            available_on=instance.available_on,
            parent_id=instance.checkout_session_id,
            reference_id=instance.reference_id,
            payment_method=instance.payment_method,
            stripe_fee=instance.stripe_fee
        )

        if instance.reference_id:
            try:
                promise_to_pay = PromiseToPay.objects.get(
                    unique_key=instance.reference_id
                )
                if promise_to_pay:
                    promise_to_pay.is_redeemed = True
                    promise_to_pay.save()
            except PromiseToPay.DoesNotExist:
                pass


@receiver(post_save, sender=Payout)
def update_transaction_payout(sender, instance, created, **kwargs):
    if instance.status == 'pending':
        instance.user.user_transaction.create(
            amount=instance.amount,
            currency=instance.currency,
            net=instance.net,
            fee=instance.fee,
            type='payout',
            status='pending',
            created_on=instance.created_on,
            parent_id=instance.payout_id
        )
    elif instance.status == 'paid':
        transaction = Transaction.objects.get(
            parent_id=instance.payout_id
        )

        transaction_id = instance.transaction_id
        print('transaction id : ', transaction_id)
        transaction.transaction_id = transaction_id
        transaction.status = 'complete'
        transaction.save()
    elif instance.status == 'failed':
        transaction = Transaction.objects.get(
            parent_id=instance.payout_id
        )
        transaction.delete()

        UserBalance.objects.get_avalible_to_payout(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_avalible_to_payout(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_avalible_to_payout_soon(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_current_month_total(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_since_last_payout(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_total_volume(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_total_balance(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_total_payout(
            transaction.user,
            transaction.user.user_balance
        )
        UserBalance.objects.get_total_fee(
            transaction.user,
            transaction.user.user_balance
        )
        transaction.user.user_balance.save()
