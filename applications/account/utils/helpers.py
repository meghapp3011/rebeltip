from django.conf import settings
import datetime
import csv
from rest_framework.response import Response
from rest_framework import status
from applications.user.models import Transaction
from django.template.loader import render_to_string
from django.core.files.storage import get_storage_class
from utils.common.helpers import send_email
import os


# def send_email(email, subject, message, html_message):
#     """
#     Send mail to the given mail address
#     """
#     from_email = settings.EMAIL_HOST_USER
#     recipient_list = [email]
#     send_mail(
#         subject, message,
#         from_email, recipient_list, html_message=html_message
#     )


def send_payout_mail(instance, email):
    subject = 'Cash Out Details:Withdrawal Processed'
    amount = '{:.2f}'.format(abs(instance.amount) / 100)
    net = '{:.2f}'.format(abs(instance.net) / 100)
    fee = '{:.2f}'.format(abs(instance.fee) / 100)
    message = render_to_string('payout_email.html', {
        'user': instance.user.first_name.capitalize(),
        'instance': instance,
        'amount': amount,
        'net': net,
        'fee': fee

    })
    send_email(
        email,
        subject=subject,
        message=message,
        html_message=message
    )

# def transaction_download(validated_data, user, request):
#     month_str = validated_data.get('month')
#     year = validated_data.get('year')

#     try:
#         month = datetime.datetime.strptime(month_str, "%B").month
#     except ValueError:
#         return Response(
#             {'error': 'Invalid month format'},
#             status=status.HTTP_400_BAD_REQUEST
#         )

#     transactions = Transaction.objects.filter(
#         created_on__year=year,
#         created_on__month=month,
#         user=user
#     ).order_by('-created_on')

#     # Create the file name
#     file_name = f"transactions_{year}_{month_str.lower()}.csv"

#     s3_file_path = os.path.join(settings.MEDIA_ROOT, 'statement', file_name)

#     with open(s3_file_path, 'w', newline='') as csv_file:
#         writer = csv.writer(csv_file)
#         writer.writerow([
#             'Date', 'Transaction ID', 'Amount', 'Currency',
#             'Net', 'Fee', 'Type', 'Status',
#         ])
#         for transaction in transactions:
#             transaction_type = (
#                 "Tip received" if transaction.type == "charge"
#                 else "Payout"
#             )
#             fee = transaction.fee
#             net = transaction.net
#             writer.writerow([
#                 transaction.created_on.date(),
#                 transaction.transaction_id,
#                 '{:.2f}'.format(transaction.amount/100),
#                 transaction.currency,
#                 '{:.2f}'.format(float(net/100)),
#                 '{:.2f}'.format(float(fee/100)),
#                 transaction_type,
#                 transaction.status,
#             ])


#     # file_url = f"{settings.MEDIA_URL}statement/{file_name}"

#     # Generate response data
#     domain = get_current_site(request)

#     protocol = settings.PROTOCOL
#     file_url = f"{protocol}://{domain}/media/statement/{file_name}"
#     response_data = {'file_url': file_url}

#     # Open the created file for sending as a response
#     file_response = FileResponse(open(s3_file_path, 'rb'))
#     file_response['Content-Disposition'] = f'attachment; filename="{file_name}"'

#     return Response(response_data, status=status.HTTP_200_OK)

def transaction_download(validated_data, user, request):
    month_str = validated_data.get('month')
    year = validated_data.get('year')

    try:
        month = datetime.datetime.strptime(month_str, "%B").month
    except ValueError:
        return Response(
            {'error': 'Invalid month format'},
            status=status.HTTP_400_BAD_REQUEST
        )

    transactions = Transaction.objects.filter(
        created_on__year=year,
        created_on__month=month,
        user=user
    ).order_by('-created_on')

    file_name = f"statement/transactions_{user.id}_{year}_{month_str.lower()}.csv"

    # Generate CSV file
    with open(file_name, 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([
            'Date', 'Transaction ID', 'Amount', 'Currency',
            'Net', 'Fee', 'Type', 'Status',
        ])
        for transaction in transactions:
            transaction_type = (
                "Tip received" if transaction.type == "charge"
                else "Payout"
            )
            fee = transaction.fee
            net = transaction.net
            writer.writerow([
                transaction.created_on.date(),
                transaction.transaction_id,
                '{:.2f}'.format(transaction.amount/100),
                transaction.currency,
                '{:.2f}'.format(float(net/100)),
                '{:.2f}'.format(float(fee/100)),
                transaction_type,
                transaction.status,
            ])

    # Upload CSV file to S3
    s3_storage = get_storage_class(settings.STORAGES['default']['BACKEND'])()
    s3_file_path = f"media/{file_name}"
    with open(file_name, 'rb') as data:
        s3_storage.save(s3_file_path, data)

    file_path_del = os.path.join('', file_name)
    os.remove(file_path_del)

    response_data = {
        'file_url': s3_storage.url(s3_file_path)
    }

    # Return response
    return Response(response_data, status=status.HTTP_200_OK)


def payout_percentage(withdrow_amount, total_amount):
    percentage = (withdrow_amount/total_amount) * 100
    return percentage


def payoutable_amount(req_percentage, total_balance):
    amount = (req_percentage/100) * total_balance
    return round(amount)
