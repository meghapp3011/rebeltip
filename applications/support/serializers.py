from rest_framework import serializers
from applications.support import models


class ContactUsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ContactUs
        fields = '__all__'


class FaqSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Faq
        fields = ['question', 'answer']


class PrivacyPolicySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.PrivacyPolicy
        fields = ['privacy_policy', 'cookie_policy']
