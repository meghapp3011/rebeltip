from django.urls import path, include
from rest_framework.routers import DefaultRouter
from applications.support import views


router = DefaultRouter()

router.register(
    r'contact-us', views.ContactUsViewSet, basename='contact_us',
)
router.register(
    r'faq', views.FaqViewSet, basename='faq'
)
router.register(
    r'privacy-policy', views.PrivacyPolicyViewSet, basename='privacy_policy'
)


urlpatterns = [
    path('', include(router.urls)),

]
