from rest_framework import viewsets, status
from applications.support import serializers
from applications.support import models
from rest_framework.response import Response

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class ContactUsViewSet(viewsets.ViewSet):
    serializer_class = serializers.ContactUsSerializer
    queryset = models.ContactUs.objects.all()

    def get_queryset(self):
        return models.ContactUs.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message": "ContactUs created successfully"},
                            status=status.HTTP_200_OK)
        return Response({"error": serializer.errors},
                        status=status.HTTP_400_BAD_REQUEST)


class FaqViewSet(viewsets.ViewSet):
    serializer_class = serializers.FaqSerializer
    # pagination_class = None

    @method_decorator(cache_page(60 * 60 * 2))
    def list(self, request):
        category = self.request.query_params.get('category')
        if category:
            queryset = models.Faq.objects.filter(category=category)
        else:
            queryset = models.Faq.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PrivacyPolicyViewSet(viewsets.ViewSet):
    serializer_class = serializers.PrivacyPolicySerializer

    # @method_decorator(cache_page(60 * 60 * 2))
    def list(self, request):
        queryset = models.PrivacyPolicy.objects.order_by('created_on').first()
        serializer = self.serializer_class(queryset)
        return Response(serializer.data, status=status.HTTP_200_OK)
