from django.contrib import admin
from applications.support import models as support_model
# Register your models here.


# admin.site.register(support_model.ContactUs)
# admin.site.register(support_model.Faq)
# admin.site.register(support_model.PrivacyPolicy)


@admin.register(support_model.ContactUs)
class ContactUsModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'phone', 'message')
    search_fields = ("name", "email", "phone")
    ordering = ('-id',)


@admin.register(support_model.Faq)
class FaqModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'category', 'question', 'answer',
                    'created_on', 'updated_on')
    list_filter = ('category',)
    search_fields = ("question", "answer")
    ordering = ('-created_on',)


@admin.register(support_model.PrivacyPolicy)
class PrivacyPolicyModelAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'privacy_policy', 'cookie_policy',
        'created_on', 'updated_on'
        )
    search_fields = ("privacy_policy",)
    ordering = ('-created_on',)
