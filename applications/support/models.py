from django.db import models
from ckeditor.fields import RichTextField

from utils.common.custom_id_field import CustomIDModel


class ContactUs(CustomIDModel):
    id_prefix = 'cus'
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, unique=True)
    phone = models.CharField(max_length=25, null=False, blank=True)
    message = models.CharField(max_length=200, null=False, blank=True)


class Faq(CustomIDModel):
    id_prefix = 'faq'
    QUESTION_CATEGORY_CHOICES = [
        ('receiving_tip', 'Receiving a Tip'),
        ('sending_tip', 'Sending a Tip'),
    ]
    question = models.TextField()
    answer = models.TextField()
    category = models.CharField(max_length=20,
                                choices=QUESTION_CATEGORY_CHOICES)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now_add=True, null=True)


class PrivacyPolicy(CustomIDModel):
    id_prefix = 'ppy'
    privacy_policy = RichTextField()
    cookie_policy = RichTextField()
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now_add=True, null=True)
